FROM ubuntu:22.04

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        gnupg libglib2.0-dev pkg-config libssl-dev gcc dpkg-dev aptly ca-certificates && \
    rm -rf /var/lib/apt/lists/*

COPY popos.gpg /tmp/popos.gpg

RUN gpg --no-default-keyring --keyring /tmp/popos.gpg --export --output /etc/apt/keyrings/popos.gpg
RUN echo "deb [signed-by=/etc/apt/keyrings/popos.gpg] http://apt.pop-os.org/release jammy main" > /etc/apt/sources.list.d/popos.list
RUN apt-get update && \
    apt-get install -y --no-install-recommends cargo && \
    rm -rf /var/lib/apt/lists/*

RUN cargo install --locked cargo-deb@2.6.1 --root /usr/local && \
    rm -rf /root/.cargo/registry/cache /root/.cargo/registry/src
