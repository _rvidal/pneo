# APT repositories

We provide an APT repository for some Debian-based distros. First you need a signing key:

```shell
sudo wget https://gitlab.com/_rvidal/pneo/raw/master/etc/pneo.asc \
  -O /etc/apt/keyrings/pneo.asc
```

For establishing trust, here are some alternative URLs for the signing key:

* https://keyserver.ubuntu.com/pks/lookup?op=index&search=0xdb474fc134f2495bd309215c481d20bdc3623995
* https://gitlab.com/_rvidal.gpg
* https://github.com/jrvidal.gpg

Then, add the repository to your sources:

```shell
# Ubuntu 22.04 jammy

echo "deb [signed-by=/etc/apt/keyrings/pneo.asc] https://_rvidal.gitlab.io/pneo/repository/ubuntu jammy main" | \
  sudo tee /etc/apt/sources.list.d/pneo.list
```

Finally, you can install `pneo` through `apt`:

```shell
sudo apt update
sudo apt install pneo
```
