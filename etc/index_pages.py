import os
import html
import sys


def generate_index_pages(start_folder, is_root=False):
    for root, dirs, files in os.walk(start_folder):
        index_path = os.path.join(root, "index.html")

        html_content = """
            <html>
            <head>
            <meta charset="utf8">
            </head>
            <body>
            <h1>
        """
        html_content = "<html><head></head><body><h1>"
        html_content += "Index of {}".format(html.escape(root))
        html_content += "</h1><hr /><ul>"

        if root != start_folder:
            html_content += '<li><a href="..">..</a></li>'

        for name in dirs + files:
            is_dir = name in dirs
            escaped = html.escape(name)
            suffix = "/" if is_dir else ""

            html_content += "<li>"
            html_content += f'<a href="{escaped}">{escaped}{suffix}</a>'
            html_content += "</li>"

        html_content += "</ul><hr /></body></html>"

        with open(index_path, "w", encoding="utf-8") as f:
            f.write(html_content)


generate_index_pages(sys.argv[1])
