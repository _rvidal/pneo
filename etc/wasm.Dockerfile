FROM rust:1.77-bookworm

RUN apt-get update && apt-get install -y --no-install-recommends nodejs npm

RUN wget https://github.com/rustwasm/wasm-pack/releases/download/v0.13.0/wasm-pack-v0.13.0-x86_64-unknown-linux-musl.tar.gz -O wasm-pack.tar.gz && \
    tar -xf wasm-pack.tar.gz && \ 
    mv wasm-pack-v0.13.0-x86_64-unknown-linux-musl/wasm-pack /usr/local/cargo/bin && \
    rm -r wasm-pack-v0.13* && \
    echo "0ae6c753850e8cd756e659a215b9582b73fd00d239c3767ae3d3b94184ea0df8  /usr/local/cargo/bin/wasm-pack" | sha256sum
