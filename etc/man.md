pneo 1 "September 2024" pneo "User Manual"
==================================================

# NAME
pneo - A TUI application to navigate INSPIRE and arXiv.

# DESCRIPTION

**pneo** (πνέω) is a TUI application to navigate [INSPIRE](https://inspirehep.net/) and [arXiv](https://arxiv.org/). It is inspired by [spires.app](https://member.ipmu.jp/yuji.tachikawa/spires/) by Yuji Tachikawa.

It allows searching the INSPIRE database, downloading and opening preprints from arXiv (using `xdg-open`) and accessing previously downloaded preprints in *offline mode*.

# PREPRINTS OUTSIDE ARXIV

By default, **pneo** allows you to download a preprint for an INSPIRE record if it detects that there is a corresponding preprint in arXiv. For records that do not have a preprint in arXiv (for instance, a proceeding or a book chapter) you can store a manually downloaded PDF file into pneo's cache. Once you have the INSPIRE record id, run:

```
pneo record <RECID> <FILE>
```

# KEYBOARD COMMANDS

* Ctrl+h           show this help

* Up, Down         select entry in results list

* Enter            open article

* Esc              close dialogs, cancel downloads, quit application

* PgUp, PgDown     jump through results list

* Ctrl+Left/Right  jump words in search input

* Ctrl+w           delete word in search input

* Ctrl+r           refresh screen

* Ctrl+o           toggle offline mode (✈)

* Ctrl+a           toggle abstract view

* Ctrl+b           copy Bibtex reference

* Ctrl+p           open entry in INSPIRE

# BUGS

https://gitlab.com/_rvidal/pneo/-/issues

# LICENSE

GPL-3.0-only

# AUTHORS

Roberto Vidal \<**roberto.vidal@ikumene.com**>
