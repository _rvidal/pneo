use std::{
    backtrace::Backtrace,
    cell::RefCell,
    ffi::{OsStr, OsString},
    io::{self, IsTerminal as _, Write},
    panic::AssertUnwindSafe,
    path::{Path, PathBuf},
    pin::Pin,
    process::Command,
    sync::Arc,
    task::Poll,
    time::{Duration, SystemTime},
};

use anyhow::{Context as _, Result};
use crossterm::{
    event::{self, EventStream},
    terminal,
};
use futures::{future::FusedFuture, Future};
use pneo_engine::{api::InspireSearch, main_loop, Cache, Runtime, Store as _, VersionChecker};
use ratatui::{backend::CrosstermBackend, Terminal};
use rusqlite::Connection;

use crate::{cache::LocalCache, store::LocalStore};

mod cache;
mod store;

const VERSION: &str = std::env!("CARGO_PKG_VERSION");

static DATA_DIR: once_cell::sync::Lazy<PathBuf> = once_cell::sync::Lazy::new(|| {
    let mut dir = dirs::data_dir().expect("unable to find suitable data directory");
    dir.push("pneo");
    dir
});

thread_local! {
    static BACKTRACE: RefCell<Option<Backtrace>> = RefCell::new(None);
}

fn main() -> anyhow::Result<()> {
    std::panic::set_hook(Box::new(|_| {
        let trace = Backtrace::force_capture();
        BACKTRACE.with(move |b| b.borrow_mut().replace(trace));
    }));

    let preprint_dir = {
        let mut dir = DATA_DIR.clone();
        dir.push("preprints");
        dir
    };

    let runtime_dir = {
        let mut dir = dirs::runtime_dir()
            .ok_or(anyhow::anyhow!("unable to find suitable runtime directory"))?;
        dir.push("pneo");
        dir
    };

    std::fs::create_dir_all(&runtime_dir).context(format!(
        "Unable to create runtime directory at {:?}",
        runtime_dir
    ))?;

    std::fs::create_dir_all(&preprint_dir).context(format!(
        "Unable to create cache directory at {:?}",
        runtime_dir
    ))?;

    let args: Vec<_> = std::env::args_os().skip(1).collect();

    if args == &["--version"] || args == &["-V"] {
        println!("pneo {}", VERSION);
        return Ok(());
    }

    let (cache_connection, store_connection) = {
        let path = {
            let mut db = DATA_DIR.clone();
            db.push("pneo.db");
            db
        };
        let get = || Connection::open(&path).context("unable to create database");

        (get()?, get()?)
    };

    let mut cache = LocalCache::new(cache_connection, preprint_dir);
    let mut store = LocalStore {
        connection: store_connection,
    };

    cache.init().context("unable to initialize database")?;
    store.init().context("unable to initialize database")?;

    if args.len() == 3 && args[0] == "record" {
        futures::executor::block_on(save_record(store, cache, &args[1], &args[2]))?;
        return Ok(());
    }
    let help_msg = format!(
        r#"Usage: pneo
       pneo record <RECID> <FILE>  # Store an article by INSPIRE record id
Options:
 -V, --version
        Print version
"#
    );

    if args == &["-h"] || args == &["--help"] {
        println!("INSPIRE TUI browser");
        println!();
        print!("{help_msg}");
        return Ok(());
    }

    if !args.is_empty() {
        eprintln!("error: unrecognized arguments");
        eprintln!();
        eprint!("{help_msg}");
        std::process::exit(1);
    }

    {
        let mut builder = env_logger::Builder::from_env(
            env_logger::Env::default().filter_or("PNEO_LOG", "pneo=info"),
        );

        let logfile = {
            let mut logfile = runtime_dir;
            logfile.push("pneo.log");
            logfile
        };

        builder.target(env_logger::Target::Pipe(Box::new(
            std::fs::File::create(logfile).context("Unable to create logfile")?,
        )));

        builder.init();
    }

    if let Err(err) = cache
        .get_downloaded()
        .and_then(|downloaded| store.try_prune(downloaded))
    {
        log::error!("Unable to prune store: {:?}", err);
    }

    fn start_terminal() -> io::Result<Terminal<CrosstermBackend<impl Write>>> {
        terminal::enable_raw_mode()?;
        let mut stdout = io::stdout();
        crossterm::execute!(
            stdout,
            terminal::EnterAlternateScreen,
            event::EnableMouseCapture
        )?;
        let backend = CrosstermBackend::new(stdout);
        let terminal = Terminal::new(backend)?;
        Ok(terminal)
    }

    let mut terminal = match start_terminal().context("unable to start terminal interface") {
        Err(e) => {
            log::debug!("{:?}", e);
            Err(e)
        }
        res => res,
    }?;

    let assert = AssertUnwindSafe((&mut terminal, cache, store));
    let result = std::panic::catch_unwind(|| {
        let assert = assert;
        main_panic_boundary(assert.0 .0, assert.0 .1, assert.0 .2)
    });

    fn stop_terminal(mut terminal: Terminal<CrosstermBackend<impl Write>>) -> io::Result<()> {
        terminal::disable_raw_mode()?;
        crossterm::execute!(
            terminal.backend_mut(),
            terminal::LeaveAlternateScreen,
            event::DisableMouseCapture
        )?;
        terminal.show_cursor()?;
        Ok(())
    }

    if let Err(e) = stop_terminal(terminal).context("unable to restore terminal") {
        log::debug!("{e:?}");
    }

    match result {
        Ok(result) => result,
        Err(any) => {
            eprintln!("main loop panicked");

            if let Some(s) = any.downcast_ref::<&str>() {
                eprintln!("{s}");
            }

            if let Some(s) = any.downcast_ref::<String>() {
                eprintln!("{s}");
            }
            let b = BACKTRACE.with(|b| b.borrow_mut().take()).unwrap();
            println!("at panic:\n{b}");

            std::process::exit(1);
        }
    }
}

async fn save_record(
    mut store: LocalStore,
    cache: LocalCache,
    recid: &OsString,
    file: &OsString,
) -> anyhow::Result<()> {
    fn parse_recid(recid: &OsStr) -> Option<u32> {
        let recid = recid.to_str()?;
        u32::from_str_radix(recid, 10).ok()
    }

    fn accept() -> anyhow::Result<bool> {
        if !io::stdin().is_terminal() {
            anyhow::bail!("`pneo record` is meant to be run interactively.");
        }

        let mut line = String::new();

        loop {
            line.truncate(0);

            print!("Continue? [Y/n] ");
            let _ = io::stdout().flush();

            let stdin = &std::io::stdin();
            stdin.read_line(&mut line)?;

            line.make_ascii_lowercase();

            if line.is_empty() {
                return Ok(false);
            }

            let answer = line.trim();

            if answer.is_empty() || answer == "y" || answer == "yes" {
                return Ok(true);
            } else if answer == "n" || answer == "no" {
                return Ok(false);
            }
        }
    }

    if !Path::new(file).exists() {
        anyhow::bail!("file {:?} does not exist", file);
    }

    let pdf = is_pdf(file).context("unable to determine if file is a PDF")?;

    if !pdf {
        anyhow::bail!("file {:?} is not a PDF", file);
    }

    let Some(recid) = parse_recid(&recid) else {
        anyhow::bail!("invalid record id {:?}", recid);
    };

    let (InspireSearch(mut articles), _) = search(format!("recid:{}", recid), 0).await?;

    if articles.is_empty() {
        anyhow::bail!("Could not find any records for recid:{}", recid);
    } else if articles.len() > 1 {
        anyhow::bail!("Found several records for recid:{}", recid);
    }

    let article = articles.pop().unwrap();

    let has_recid = cache.get_recid(recid)?.is_some();

    println!("Storing INSPIRE record...");
    println!();
    println!("INSPIRE record id: {}", recid);

    if let Some(title) = &article.title {
        println!("Title: {}", title);
    }

    if !article.authors.is_empty() {
        println!("Authors: {}", article.authors.join(", "))
    }

    println!("Date: {}", article.created);

    if has_recid {
        println!();
        println!("Already in cache, it will be replaced.");
    }

    println!();

    if accept()? {
        cache.insert_recid(recid, file)?;
        // better to do after, in case file path is not correct
        store.upsert(&article)?;
    }

    Ok(())
}

fn main_panic_boundary<B: ratatui::backend::Backend>(
    terminal: &mut Terminal<B>,
    cache: LocalCache,
    store: LocalStore,
) -> anyhow::Result<()> {
    let runtime = tokio::runtime::Builder::new_current_thread()
        .enable_time()
        .enable_io()
        .build()
        .context("unable to start runtime")?;

    let _guard = runtime.enter();

    struct LocalRuntime {
        cache: Arc<LocalCache>,
        store: Option<LocalStore>,
        // runtime: tokio::runtime::Runtime,
    }

    struct LocalVersionChecker;

    impl VersionChecker for LocalVersionChecker {
        fn current(&self) -> &str {
            VERSION
        }

        fn update(&self) {
            // TODO
        }

        fn last(&self) -> io::Result<Option<SystemTime>> {
            // TODO
            Ok(None)
        }
    }

    impl Runtime for LocalRuntime {
        type Cache = LocalCache;

        type Store = LocalStore;

        type VersionChecker = LocalVersionChecker;

        fn cache(&self) -> Arc<Self::Cache> {
            self.cache.clone()
        }

        fn store(&mut self) -> Option<Self::Store> {
            self.store.take()
        }

        fn version_checker(&self) -> Self::VersionChecker {
            LocalVersionChecker
        }

        fn open(&self, uri: impl AsRef<std::ffi::OsStr> + std::fmt::Debug) -> Result<()> {
            xdg_open(uri)
        }

        fn copy(&self, content: &str) -> Result<()> {
            xclip(content)
        }

        fn offline(&self) -> bool {
            let nm = gio::NetworkMonitor::default();

            gio::traits::NetworkMonitorExt::connectivity(&nm) != gio::NetworkConnectivity::Full
        }

        fn tick(millis: u64) -> impl Future<Output = ()> + 'static {
            tokio::time::sleep(Duration::from_millis(millis))
        }
    }

    runtime.block_on(main_loop(
        terminal,
        LocalRuntime {
            cache: Arc::new(cache),
            store: Some(store),
            // runtime,
        },
        EventStream::new(),
    ))
}

struct InspectPoll<F> {
    fut: F,
    name: &'static str,
}

impl<F: Future> Future for InspectPoll<F> {
    type Output = F::Output;

    fn poll(self: Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        let name = self.name;
        log::debug!("polling {:?}", name);
        let this = unsafe { self.get_unchecked_mut() };
        let poll = unsafe { Pin::new_unchecked(&mut this.fut) }.poll(cx);
        log::debug!("polled {:?} with ready = {}", name, poll.is_ready());
        poll
    }
}

impl<F: FusedFuture> FusedFuture for InspectPoll<F> {
    fn is_terminated(&self) -> bool {
        log::debug!(
            "is {:?} terminated? = {}",
            self.name,
            self.fut.is_terminated()
        );
        self.fut.is_terminated()
    }
}

async fn search(input: String, page: usize) -> Result<(InspireSearch, usize)> {
    let res = pneo_engine::api::search_inspire(input, page).await?;

    Ok((res, page))
}

fn xdg_open(path: impl AsRef<std::ffi::OsStr> + std::fmt::Debug) -> Result<()> {
    log::info!("opening {:?}", path);

    let mut child = Command::new("xdg-open")
        .arg(path)
        .stdin(std::process::Stdio::null())
        .stdout(std::process::Stdio::piped())
        .stderr(std::process::Stdio::piped())
        .spawn()
        .context("unable to open")?;

    let status = child.wait()?;

    if status.success() {
        return Ok(());
    }

    let output = child.wait_with_output()?;

    let stdout = String::from_utf8_lossy(&output.stdout);
    let stderr = String::from_utf8_lossy(&output.stderr);

    let error = anyhow::anyhow!(
        "unable to open, xdg-open failed with {}\n{}\n{}",
        status,
        stdout,
        stderr
    );

    log::error!("{:?}", error);

    Err(error)
}

fn xclip(data: &str) -> Result<()> {
    let mut child = Command::new("xclip")
        .args(&["-selection", "clipboard"])
        .stdin(std::process::Stdio::piped())
        .stdout(std::process::Stdio::piped())
        .stderr(std::process::Stdio::piped())
        .spawn()
        .context("unable to spawn xclip")?;

    let stdin = child
        .stdin
        .as_mut()
        .ok_or_else(|| anyhow::anyhow!("unable to send data to xclip"))?;

    stdin
        .write_all(data.as_bytes())
        .context("writing to xclip")?;

    let status = child.wait()?;

    if status.success() {
        return Ok(());
    }

    let output = child.wait_with_output()?;

    let stdout = String::from_utf8_lossy(&output.stdout);
    let stderr = String::from_utf8_lossy(&output.stderr);

    let error = anyhow::anyhow!(
        "unable to copy, xclip failed with {}\n{}\n{}",
        status,
        stdout,
        stderr
    );

    log::error!("{:?}", error);

    Err(error)
}

fn is_pdf(filepath: &OsStr) -> Result<bool> {
    let mut buffer = [0; 7];

    let mut file = std::fs::File::open(filepath)?;

    io::Read::read(&mut file, &mut buffer)?;

    Ok(&buffer == b"%PDF-1.")
}
