use std::collections::HashMap;

use anyhow::Result;
use pneo_engine::{
    syntax::{FieldKind, Operator, Query, SimpleQuery},
    Article, Download, Store,
};
use rusqlite::{Connection, ToSql};

pub struct LocalStore {
    pub connection: Connection,
}

impl LocalStore {
    pub fn init(&self) -> Result<()> {
        self.connection.execute(TABLE, ())?;

        Ok(())
    }

    pub fn try_prune(&mut self, eprints: HashMap<String, Download>) -> Result<()> {
        let tx = self.connection.transaction()?;

        tx.execute("ALTER TABLE records ADD COLUMN keep INT DEFAULT 0", ())?;

        let mut stmt = tx.prepare("UPDATE records SET keep = 1 WHERE eprint = ?")?;

        for (reference, download) in &eprints {
            if std::matches![download, Download::Arxiv(_)] {
                let _ = stmt.execute(rusqlite::params![&reference]);
            }
        }

        drop(stmt);

        let mut stmt = tx.prepare("UPDATE records SET keep = 1 WHERE control_number = ?")?;

        for (reference, download) in &eprints {
            if std::matches![download, Download::RecId] {
                let _ = stmt.execute(rusqlite::params![&reference]);
            }
        }

        drop(stmt);

        let deleted = tx.execute("DELETE FROM records WHERE keep != 1", ())?;

        log::info!("pruned {} records", deleted);

        let _ = tx.execute("ALTER TABLE records DROP COLUMN keep", ())?;

        tx.commit()?;

        Ok(())
    }
}

impl Store for LocalStore {
    fn upsert(&mut self, article: &Article) -> Result<()> {
        let tx = self.connection.transaction()?;

        let mut stmt = tx.prepare_cached(
            r#"
                INSERT INTO records (control_number, title, authors, created, bibtex, eprint) VALUES (?, ?, ?, ?, ?, ?)
                ON CONFLICT (control_number) DO UPDATE SET
                    title=excluded.title, authors=excluded.authors, created=excluded.created, bibtex=excluded.bibtex, eprint=excluded.eprint
            "#,
        )?;

        // for article in records {
        stmt.execute(rusqlite::params![
            article.control_number,
            article.title,
            json_row(&article.authors)?,
            article.created,
            article.bibtex,
            article.eprints.first(),
        ])?;
        // }

        drop(stmt);
        tx.commit()?;

        Ok(())
    }

    fn query(&self, query: Query) -> Result<Vec<Article>> {
        let mut stmt = "SELECT * FROM records WHERE ".to_string();

        let (clause, params) = query_to_clause(query);

        stmt.push_str(&clause);

        stmt.push_str(" LIMIT 50");

        log::debug!("query with {:?}", stmt);
        let mut stmt = self.connection.prepare(&stmt)?;

        let mapped_rows = stmt.query_map(rusqlite::params_from_iter(params), |row| {
            RawRecord::try_from(row)
        })?;

        let result: Vec<_> = mapped_rows
            .map(|raw| raw.map_err(anyhow::Error::from))
            .map(|raw| raw.and_then(|raw| Ok(Article::try_from(raw)?)))
            .collect::<Result<_, _>>()?;

        Ok(result)
    }
}

struct RawRecord {
    pub control_number: u32,
    pub title: Option<String>,
    pub authors: String,
    pub created: String,
    pub bibtex: String,
    pub eprint: Option<String>,
}

impl TryFrom<&'_ rusqlite::Row<'_>> for RawRecord {
    type Error = rusqlite::Error;

    fn try_from(row: &'_ rusqlite::Row<'_>) -> rusqlite::Result<Self> {
        Ok(RawRecord {
            control_number: row.get("control_number")?,
            title: row.get("title")?,
            created: row.get("created")?,
            authors: row.get("authors")?,
            bibtex: row.get("bibtex")?,
            eprint: row.get("eprint")?,
        })
    }
}

impl TryFrom<RawRecord> for Article {
    type Error = serde_json::Error;

    fn try_from(value: RawRecord) -> Result<Self, Self::Error> {
        Ok(Self {
            control_number: value.control_number,
            title: value.title,
            authors: serde_json::from_str(&value.authors)?,
            created: value.created,
            bibtex: value.bibtex,
            eprints: value.eprint.into_iter().collect(),
            abstracts: vec![],
        })
    }
}

fn json_row(row: &[String]) -> Result<String> {
    Ok(serde_json::to_string(row)?)
}

const TABLE: &str = &r#"
        CREATE TABLE IF NOT EXISTS records (
          control_number INT NOT NULL,
          version INT DEFAULT 2,
          title TEXT,
          authors TEXT NOT NULL,
          created TEXT NOT NULL,
          bibtex TEXT NOT NULL,
          eprint TEXT,
          CONSTRAINT identifier UNIQUE (control_number)
        )
    "#;

fn query_to_clause(query: Query) -> (String, Vec<Box<dyn ToSql>>) {
    fn pattern(term: String) -> String {
        format!("%{}%", term)
    }

    fn build_query_to_clause(query: Query, clause: &mut String, params: &mut Vec<Box<dyn ToSql>>) {
        log::debug!("query: {:?}", query);
        match query.simple {
            SimpleQuery::Wrapped(query) => {
                clause.push_str("(");
                build_query_to_clause(*query, clause, params);
                clause.push_str(")");
            }
            SimpleQuery::Field(kind, terms) => match kind {
                FieldKind::Author => {
                    let len = terms.len();

                    for (i, term) in terms.into_iter().enumerate() {
                        // FIXME: collation?
                        clause.push_str("( authors LIKE ? )");
                        params.push(Box::new(pattern(term)));
                        if i < len - 1 {
                            clause.push_str(" AND ");
                        }
                    }
                }
                FieldKind::Title => {
                    let len = terms.len();

                    for (i, term) in terms.into_iter().enumerate() {
                        clause.push_str("( title LIKE ? )");
                        params.push(Box::new(pattern(term)));
                        if i < len - 1 {
                            clause.push_str(" AND ");
                        }
                    }
                }
            },
            SimpleQuery::Date(date) => {
                clause.push_str("( created LIKE ? )");
                params.push(Box::new(format!("{}-%", date)));
            }
        }

        if let Some((op, nested)) = query.compound {
            match op {
                Operator::And => {
                    clause.push_str(" AND (");
                }
                Operator::Or => {
                    clause.push_str(" OR (");
                }
            }

            build_query_to_clause(*nested, clause, params);

            clause.push_str(")");
        }
    }

    let mut clause = String::new();
    let mut params = vec![];

    build_query_to_clause(query, &mut clause, &mut params);

    (clause, params)
}
