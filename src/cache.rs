use std::{collections::HashMap, path::PathBuf};

use anyhow::{Context, Result};
use pneo_engine::{preprint, Cache, Download};
use rusqlite::{Connection, OptionalExtension};

pub struct LocalCache {
    connection: Connection,
    root: PathBuf,
}

impl LocalCache {
    pub fn new(connection: Connection, root: PathBuf) -> Self {
        Self { connection, root }
    }

    pub fn init(&mut self) -> Result<()> {
        let recids = {
            let mut path = self.root.clone();
            path.push("recids");
            path
        };

        std::fs::create_dir_all(&recids).context(format!(
            "Unable to create data directory at {:?}",
            self.root
        ))?;

        let _ = self
            .connection
            .execute(
                "CREATE TABLE IF NOT EXISTS eprints \
                (id TEXT NOT NULL, version INT NOT NULL, \
                    CONSTRAINT versions UNIQUE (id, version)\
                )",
                (),
            )
            .context("unable to create table")?;

        let _ = self
            .connection
            .execute(
                "CREATE TABLE IF NOT EXISTS recids \
                (recid INT NOT NULL, \
                    CONSTRAINT recid_id UNIQUE (recid)\
                )",
                (),
            )
            .context("unable to create table")?;

        Ok(())
    }

    fn from_downloaded(&self, id: &str, version: u8) -> Result<PathBuf> {
        let filename = preprint::id_to_file(id, version);

        let filepath = {
            let mut path = self.root.clone();
            path.push(filename);
            path
        };

        if !std::path::Path::new(&filepath).exists() {
            anyhow::bail!("inconsistent cache, unable to find {:?}", filepath);
        }

        Ok(filepath)
    }

    fn record_path(&self, recid: u32) -> PathBuf {
        let mut path = self.root.clone();
        path.push("recids");
        path.push(format!("{}.pdf", recid));

        path
    }
}

impl Cache for LocalCache {
    fn preprint_file_from_id(&self, id: &str) -> Result<Option<PathBuf>> {
        let mut stmt = self.connection.prepare_cached(
            "SELECT version FROM eprints WHERE id = ? ORDER BY version DESC LIMIT 1",
        )?;

        let version: Option<u8> = stmt.query_row(&[id], |row| row.get(0)).optional()?;

        let Some(version) = version else {
            return Ok(None);
        };

        self.from_downloaded(id, version).map(Some)
    }

    fn has(&self, id: &str, referenced_id: &str, url: &str) -> Result<Option<PathBuf>> {
        let (_basename, version) = preprint::validate(id, referenced_id, url)?;

        let mut stmt = self
            .connection
            .prepare_cached("SELECT 1 FROM eprints WHERE id = ? AND version = ?")?;

        let res = stmt
            .query_row(rusqlite::params![referenced_id, version], |_| Ok(()))
            .optional()?;

        if res.is_none() {
            return Ok(None);
        }

        self.from_downloaded(referenced_id, version).map(Some)
    }

    fn insert(
        &self,
        id: &str,
        referenced_id: &str,
        url: &str,
        content: Vec<u8>,
    ) -> Result<PathBuf> {
        let (basename, version) = preprint::validate(id, referenced_id, url)?;

        log::debug!(
            "(basename, version) = ({:?}, {:?}) from id = {:?}, referenced_id = {:?}, url = {:?}",
            basename,
            version,
            id,
            referenced_id,
            url
        );

        let path = {
            let mut path = self.root.clone();
            path.push(format!("{}.pdf", basename));
            path
        };

        std::fs::write(&path, content).context("unable to save preprint file")?;

        let mut stmt = self
            .connection
            .prepare_cached("INSERT INTO eprints (id, version) VALUES (?, ?)")?;

        let _ = stmt.execute(rusqlite::params![referenced_id, version])?;

        Ok(path)
    }

    fn get_downloaded(&self) -> Result<HashMap<String, Download>> {
        let mut stmt = self
            .connection
            .prepare_cached("SELECT id, MAX(version) FROM eprints GROUP BY id")?;

        let mut eprints: HashMap<String, Download> = stmt
            .query_map((), |row| Ok((row.get(0)?, Download::Arxiv(row.get(1)?))))?
            .collect::<Result<_, _>>()?;

        let mut stmt = self.connection.prepare_cached("SELECT recid FROM recids")?;

        let recids: Vec<u32> = stmt
            .query_map([], |row| Ok(row.get(0)?))?
            .collect::<Result<_, _>>()?;

        eprints.extend(
            recids
                .into_iter()
                .map(|recid| (recid.to_string(), Download::RecId)),
        );

        Ok(eprints)
    }

    fn get_recid(&self, recid: u32) -> Result<Option<PathBuf>> {
        let mut stmt = self
            .connection
            .prepare_cached("SELECT 1 FROM recids WHERE recid = ?")?;

        let exists = stmt.query_row(&[&recid], |_| Ok(())).optional()?;

        Ok(exists.map(|_| self.record_path(recid)))
    }

    fn insert_recid(&self, recid: u32, file: &std::ffi::OsString) -> anyhow::Result<()> {
        std::fs::copy(file, self.record_path(recid))?;

        let mut stmt = self.connection.prepare_cached(
            "INSERT INTO recids (recid) VALUES (?)
                ON CONFLICT (recid) DO UPDATE SET recid=excluded.recid",
        )?;

        stmt.execute(&[&recid])?;

        Ok(())
    }
}
