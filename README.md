# πνέω

A TUI application to navigate [INSPIRE](https://inspirehep.net/) and [arXiv](https://arxiv.org/). Inspired by [spires.app](https://member.ipmu.jp/yuji.tachikawa/spires/).

<div align="center"><h2>[Try the online demo](https://_rvidal.gitlab.io/pneo/)</h2></div>

![pneo demo](demo.mp4)

## Install

We provide pre-built binaries for Debian distros in [Releases](https://gitlab.com/_rvidal/pneo/-/releases). You can also install pneo through [APT](etc/apt.md).

### Building

`pneo` is built with [Rust](https://www.rust-lang.org), so you need a [Rust toolchain](https://www.rust-lang.org/tools/install). After that, it should be enough to run:

```
cargo install --path .
```

`pneo` requires Rust version 1.77 or greater.

### Dependencies
`pneo` is Linux only, and requires [gio](https://docs.gtk.org/gio/). For instance, in a Debian-like system:
```bash
# `xclip`: interact with the clipboard
# `xdg-utils`: open preprint files and URLs
apt install libglib2.0-0 xclip xdg-utils
```

## How it works

Check `pneo --help`.

* Write into the text input, arrow keys `🠈`, `🠊` to move the cursor, `Ctrl+🠈` and `Ctrl+🠊` to move words, `Ctrl+w` deletes words.
* Arrow keys `🠉`, `🠋` to select an entry, `Enter` to download/open it.
  * `PgDn`, `PgUp`, mouse scrolling also work on the list.
  * Double click works for downloading.
* `Ctrl+r` refreshes the whole screen, in case something interferes with terminal output.
* `Ctrl+a` toggles "abstract view".
* `Ctrl+b` attempts to copy the bibtex reference to the clipboard (`xclip` is needed).
* `Ctrl+o` triggers [offline mode](#offline-mode).
* `Ctrl+p` opens the article in INSPIRE.
* `Ctrl+h` shows the help dialog.
* `Esc` to close dialogs, cancel downloads and/or exit.

## Manual downloads

`pneo` is able to automatically download preprints from arXiv (and will even check for newer versions of preprints already downloaded). Articles without preprint links cannot be downloaded automatically. However, if you are able to download it from some other source, you can make `pneo` store it in its internal cache.

Once you have a PDF for the article, you can run:
```
pneo record <record id> <article pdf file>
```

## Troubleshooting

`pneo` writes logs to `$XDG_RUNTIME_DIR/pneo/pneo.log`. You can increase their verbosity with the
`PNEO_LOG` environment variable, e.g. running with `PNEO_LOG=debug` or `PNEO_LOG=trace`.

## Offline mode

Offline mode relies on cached searches and downloaded eprints: i.e. it will only show eprints that have been
opened previously. The search syntax supports just a tiny subset of INSPIRE's keyword-based searching:

```bash
# By author

au ellis

# By title

t black hole

# By exact year

date 2015

# Logical operators

(au ellis) and (t black hole)

# Initial `find` gets ignored

find (author ellis or (t black hole))


# Precedence of logical operators is similar to INSPIRES.
# These are equivalent:

au ellis and t black hole and date 2020
au ellis and (t black hole and date 2020)
```
