// import wasm from "vite-plugin-wasm";
import { defineConfig } from "vite";

export default defineConfig({
  base: "",
  server: {
    fs: {
      // Allow serving files from one level up to the project root
      allow: [".."],
    },
  },
  build: {
    target: "esnext",
  },
});
