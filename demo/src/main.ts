import "@xterm/xterm/css/xterm.css";
import "firacode/distr/fira_code.css";
import init, { main } from "../../pneo-wasm/pkg";
import { Terminal } from "@xterm/xterm";
import { FitAddon } from "@xterm/addon-fit";

const initialization = init();

const terminal = new Terminal({
  fontFamily: "Fira Code, monospace",
});

const parent = document.getElementById("terminal")!;

terminal.open(parent);

const fitAddon = new FitAddon();
terminal.loadAddon(fitAddon);

(window as any).terminal = terminal;

await initialization;

main(terminal);

Promise.resolve().then(() => {
  terminal.input("t neutrinos");
});

terminal.focus();

fitAddon.fit();

const observer = new ResizeObserver(() => {
  fitAddon.fit();
});

observer.observe(parent);
