#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 14
#define STATE_COUNT 26
#define LARGE_STATE_COUNT 4
#define SYMBOL_COUNT 30
#define ALIAS_COUNT 0
#define TOKEN_COUNT 21
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 0
#define MAX_ALIAS_SEQUENCE_LENGTH 3
#define PRODUCTION_ID_COUNT 1

enum {
  aux_sym__find_token1 = 1,
  anon_sym_f = 2,
  anon_sym_F = 3,
  anon_sym_LPAREN = 4,
  anon_sym_RPAREN = 5,
  aux_sym_date_keyword_token1 = 6,
  aux_sym_date_keyword_token2 = 7,
  sym_date_literal = 8,
  anon_sym_a = 9,
  anon_sym_A = 10,
  aux_sym_field_token1 = 11,
  anon_sym_t = 12,
  anon_sym_T = 13,
  aux_sym_field_token2 = 14,
  aux_sym_op_token1 = 15,
  aux_sym_op_token2 = 16,
  anon_sym_PLUS = 17,
  anon_sym_PIPE = 18,
  anon_sym_AMP = 19,
  sym_identifier = 20,
  sym_query = 21,
  sym__query = 22,
  sym__find = 23,
  sym__simple_query = 24,
  sym_date_query = 25,
  sym_date_keyword = 26,
  sym_field = 27,
  sym_op = 28,
  aux_sym__term = 29,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [aux_sym__find_token1] = "_find_token1",
  [anon_sym_f] = "f",
  [anon_sym_F] = "F",
  [anon_sym_LPAREN] = "(",
  [anon_sym_RPAREN] = ")",
  [aux_sym_date_keyword_token1] = "date_keyword_token1",
  [aux_sym_date_keyword_token2] = "date_keyword_token2",
  [sym_date_literal] = "date_literal",
  [anon_sym_a] = "a",
  [anon_sym_A] = "A",
  [aux_sym_field_token1] = "field_token1",
  [anon_sym_t] = "t",
  [anon_sym_T] = "T",
  [aux_sym_field_token2] = "field_token2",
  [aux_sym_op_token1] = "op_token1",
  [aux_sym_op_token2] = "op_token2",
  [anon_sym_PLUS] = "+",
  [anon_sym_PIPE] = "|",
  [anon_sym_AMP] = "&",
  [sym_identifier] = "identifier",
  [sym_query] = "query",
  [sym__query] = "_query",
  [sym__find] = "_find",
  [sym__simple_query] = "_simple_query",
  [sym_date_query] = "date_query",
  [sym_date_keyword] = "date_keyword",
  [sym_field] = "field",
  [sym_op] = "op",
  [aux_sym__term] = "_term",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [aux_sym__find_token1] = aux_sym__find_token1,
  [anon_sym_f] = anon_sym_f,
  [anon_sym_F] = anon_sym_F,
  [anon_sym_LPAREN] = anon_sym_LPAREN,
  [anon_sym_RPAREN] = anon_sym_RPAREN,
  [aux_sym_date_keyword_token1] = aux_sym_date_keyword_token1,
  [aux_sym_date_keyword_token2] = aux_sym_date_keyword_token2,
  [sym_date_literal] = sym_date_literal,
  [anon_sym_a] = anon_sym_a,
  [anon_sym_A] = anon_sym_A,
  [aux_sym_field_token1] = aux_sym_field_token1,
  [anon_sym_t] = anon_sym_t,
  [anon_sym_T] = anon_sym_T,
  [aux_sym_field_token2] = aux_sym_field_token2,
  [aux_sym_op_token1] = aux_sym_op_token1,
  [aux_sym_op_token2] = aux_sym_op_token2,
  [anon_sym_PLUS] = anon_sym_PLUS,
  [anon_sym_PIPE] = anon_sym_PIPE,
  [anon_sym_AMP] = anon_sym_AMP,
  [sym_identifier] = sym_identifier,
  [sym_query] = sym_query,
  [sym__query] = sym__query,
  [sym__find] = sym__find,
  [sym__simple_query] = sym__simple_query,
  [sym_date_query] = sym_date_query,
  [sym_date_keyword] = sym_date_keyword,
  [sym_field] = sym_field,
  [sym_op] = sym_op,
  [aux_sym__term] = aux_sym__term,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [aux_sym__find_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_f] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_F] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RPAREN] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_date_keyword_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_date_keyword_token2] = {
    .visible = false,
    .named = false,
  },
  [sym_date_literal] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_a] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_A] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_field_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_t] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_T] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_field_token2] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_op_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_op_token2] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_PLUS] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PIPE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_AMP] = {
    .visible = true,
    .named = false,
  },
  [sym_identifier] = {
    .visible = true,
    .named = true,
  },
  [sym_query] = {
    .visible = true,
    .named = true,
  },
  [sym__query] = {
    .visible = false,
    .named = true,
  },
  [sym__find] = {
    .visible = false,
    .named = true,
  },
  [sym__simple_query] = {
    .visible = false,
    .named = true,
  },
  [sym_date_query] = {
    .visible = true,
    .named = true,
  },
  [sym_date_keyword] = {
    .visible = true,
    .named = true,
  },
  [sym_field] = {
    .visible = true,
    .named = true,
  },
  [sym_op] = {
    .visible = true,
    .named = true,
  },
  [aux_sym__term] = {
    .visible = false,
    .named = false,
  },
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static const TSStateId ts_primary_state_ids[STATE_COUNT] = {
  [0] = 0,
  [1] = 1,
  [2] = 2,
  [3] = 2,
  [4] = 4,
  [5] = 5,
  [6] = 5,
  [7] = 7,
  [8] = 8,
  [9] = 9,
  [10] = 8,
  [11] = 9,
  [12] = 12,
  [13] = 12,
  [14] = 14,
  [15] = 15,
  [16] = 16,
  [17] = 17,
  [18] = 18,
  [19] = 18,
  [20] = 20,
  [21] = 21,
  [22] = 22,
  [23] = 23,
  [24] = 24,
  [25] = 25,
};

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(20);
      if (lookahead == '&') ADVANCE(43);
      if (lookahead == '(') ADVANCE(24);
      if (lookahead == ')') ADVANCE(25);
      if (lookahead == '+') ADVANCE(41);
      if (lookahead == 'A') ADVANCE(30);
      if (lookahead == 'F') ADVANCE(23);
      if (lookahead == 'T') ADVANCE(34);
      if (lookahead == 'a') ADVANCE(29);
      if (lookahead == 'f') ADVANCE(22);
      if (lookahead == 't') ADVANCE(33);
      if (lookahead == '|') ADVANCE(42);
      if (lookahead == 'D' ||
          lookahead == 'd') ADVANCE(1);
      if (lookahead == 'O' ||
          lookahead == 'o') ADVANCE(11);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(0)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(17);
      END_STATE();
    case 1:
      if (lookahead == 'A' ||
          lookahead == 'a') ADVANCE(13);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(26);
      END_STATE();
    case 2:
      if (lookahead == 'D' ||
          lookahead == 'd') ADVANCE(21);
      END_STATE();
    case 3:
      if (lookahead == 'D' ||
          lookahead == 'd') ADVANCE(39);
      END_STATE();
    case 4:
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(27);
      END_STATE();
    case 5:
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(35);
      END_STATE();
    case 6:
      if (lookahead == 'H' ||
          lookahead == 'h') ADVANCE(10);
      END_STATE();
    case 7:
      if (lookahead == 'L' ||
          lookahead == 'l') ADVANCE(5);
      END_STATE();
    case 8:
      if (lookahead == 'N' ||
          lookahead == 'n') ADVANCE(2);
      END_STATE();
    case 9:
      if (lookahead == 'N' ||
          lookahead == 'n') ADVANCE(3);
      END_STATE();
    case 10:
      if (lookahead == 'O' ||
          lookahead == 'o') ADVANCE(12);
      END_STATE();
    case 11:
      if (lookahead == 'R' ||
          lookahead == 'r') ADVANCE(37);
      END_STATE();
    case 12:
      if (lookahead == 'R' ||
          lookahead == 'r') ADVANCE(31);
      END_STATE();
    case 13:
      if (lookahead == 'T' ||
          lookahead == 't') ADVANCE(4);
      END_STATE();
    case 14:
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(14)
      if (lookahead != 0 &&
          lookahead > ',' &&
          (lookahead < '.' || '@' < lookahead) &&
          (lookahead < '[' || '`' < lookahead) &&
          (lookahead < '{' || 127 < lookahead)) ADVANCE(47);
      END_STATE();
    case 15:
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(28);
      END_STATE();
    case 16:
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(15);
      END_STATE();
    case 17:
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(16);
      END_STATE();
    case 18:
      if (eof) ADVANCE(20);
      if (lookahead == '&') ADVANCE(43);
      if (lookahead == ')') ADVANCE(25);
      if (lookahead == '+') ADVANCE(41);
      if (lookahead == '|') ADVANCE(42);
      if (lookahead == 'A' ||
          lookahead == 'a') ADVANCE(45);
      if (lookahead == 'O' ||
          lookahead == 'o') ADVANCE(46);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(18)
      if (lookahead != 0 &&
          lookahead > ',' &&
          (lookahead < '.' || '@' < lookahead) &&
          (lookahead < '[' || '`' < lookahead) &&
          (lookahead < '{' || 127 < lookahead)) ADVANCE(47);
      END_STATE();
    case 19:
      if (eof) ADVANCE(20);
      if (lookahead == '&') ADVANCE(43);
      if (lookahead == ')') ADVANCE(25);
      if (lookahead == '+') ADVANCE(41);
      if (lookahead == '|') ADVANCE(42);
      if (lookahead == 'A' ||
          lookahead == 'a') ADVANCE(9);
      if (lookahead == 'O' ||
          lookahead == 'o') ADVANCE(11);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(19)
      END_STATE();
    case 20:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 21:
      ACCEPT_TOKEN(aux_sym__find_token1);
      END_STATE();
    case 22:
      ACCEPT_TOKEN(anon_sym_f);
      if (lookahead == 'I' ||
          lookahead == 'i') ADVANCE(8);
      END_STATE();
    case 23:
      ACCEPT_TOKEN(anon_sym_F);
      if (lookahead == 'I' ||
          lookahead == 'i') ADVANCE(8);
      END_STATE();
    case 24:
      ACCEPT_TOKEN(anon_sym_LPAREN);
      END_STATE();
    case 25:
      ACCEPT_TOKEN(anon_sym_RPAREN);
      END_STATE();
    case 26:
      ACCEPT_TOKEN(aux_sym_date_keyword_token1);
      END_STATE();
    case 27:
      ACCEPT_TOKEN(aux_sym_date_keyword_token2);
      END_STATE();
    case 28:
      ACCEPT_TOKEN(sym_date_literal);
      END_STATE();
    case 29:
      ACCEPT_TOKEN(anon_sym_a);
      if (lookahead == 'U' ||
          lookahead == 'u') ADVANCE(32);
      END_STATE();
    case 30:
      ACCEPT_TOKEN(anon_sym_A);
      if (lookahead == 'U' ||
          lookahead == 'u') ADVANCE(32);
      END_STATE();
    case 31:
      ACCEPT_TOKEN(aux_sym_field_token1);
      END_STATE();
    case 32:
      ACCEPT_TOKEN(aux_sym_field_token1);
      if (lookahead == 'T' ||
          lookahead == 't') ADVANCE(6);
      END_STATE();
    case 33:
      ACCEPT_TOKEN(anon_sym_t);
      if (lookahead == 'I' ||
          lookahead == 'i') ADVANCE(36);
      END_STATE();
    case 34:
      ACCEPT_TOKEN(anon_sym_T);
      if (lookahead == 'I' ||
          lookahead == 'i') ADVANCE(36);
      END_STATE();
    case 35:
      ACCEPT_TOKEN(aux_sym_field_token2);
      END_STATE();
    case 36:
      ACCEPT_TOKEN(aux_sym_field_token2);
      if (lookahead == 'T' ||
          lookahead == 't') ADVANCE(7);
      END_STATE();
    case 37:
      ACCEPT_TOKEN(aux_sym_op_token1);
      END_STATE();
    case 38:
      ACCEPT_TOKEN(aux_sym_op_token1);
      if (lookahead != 0 &&
          lookahead > ',' &&
          (lookahead < '.' || '@' < lookahead) &&
          (lookahead < '[' || '`' < lookahead) &&
          (lookahead < '{' || 127 < lookahead)) ADVANCE(47);
      END_STATE();
    case 39:
      ACCEPT_TOKEN(aux_sym_op_token2);
      END_STATE();
    case 40:
      ACCEPT_TOKEN(aux_sym_op_token2);
      if (lookahead != 0 &&
          lookahead > ',' &&
          (lookahead < '.' || '@' < lookahead) &&
          (lookahead < '[' || '`' < lookahead) &&
          (lookahead < '{' || 127 < lookahead)) ADVANCE(47);
      END_STATE();
    case 41:
      ACCEPT_TOKEN(anon_sym_PLUS);
      END_STATE();
    case 42:
      ACCEPT_TOKEN(anon_sym_PIPE);
      END_STATE();
    case 43:
      ACCEPT_TOKEN(anon_sym_AMP);
      END_STATE();
    case 44:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'D' ||
          lookahead == 'd') ADVANCE(40);
      if (lookahead != 0 &&
          lookahead > ',' &&
          (lookahead < '.' || '@' < lookahead) &&
          (lookahead < '[' || '`' < lookahead) &&
          (lookahead < '{' || 127 < lookahead)) ADVANCE(47);
      END_STATE();
    case 45:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'N' ||
          lookahead == 'n') ADVANCE(44);
      if (lookahead != 0 &&
          lookahead > ',' &&
          (lookahead < '.' || '@' < lookahead) &&
          (lookahead < '[' || '`' < lookahead) &&
          (lookahead < '{' || 127 < lookahead)) ADVANCE(47);
      END_STATE();
    case 46:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'R' ||
          lookahead == 'r') ADVANCE(38);
      if (lookahead != 0 &&
          lookahead > ',' &&
          (lookahead < '.' || '@' < lookahead) &&
          (lookahead < '[' || '`' < lookahead) &&
          (lookahead < '{' || 127 < lookahead)) ADVANCE(47);
      END_STATE();
    case 47:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead != 0 &&
          lookahead > ',' &&
          (lookahead < '.' || '@' < lookahead) &&
          (lookahead < '[' || '`' < lookahead) &&
          (lookahead < '{' || 127 < lookahead)) ADVANCE(47);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 0},
  [2] = {.lex_state = 0},
  [3] = {.lex_state = 0},
  [4] = {.lex_state = 0},
  [5] = {.lex_state = 0},
  [6] = {.lex_state = 0},
  [7] = {.lex_state = 0},
  [8] = {.lex_state = 18},
  [9] = {.lex_state = 18},
  [10] = {.lex_state = 18},
  [11] = {.lex_state = 18},
  [12] = {.lex_state = 19},
  [13] = {.lex_state = 19},
  [14] = {.lex_state = 19},
  [15] = {.lex_state = 19},
  [16] = {.lex_state = 0},
  [17] = {.lex_state = 0},
  [18] = {.lex_state = 14},
  [19] = {.lex_state = 14},
  [20] = {.lex_state = 0},
  [21] = {.lex_state = 0},
  [22] = {.lex_state = 0},
  [23] = {.lex_state = 0},
  [24] = {.lex_state = 0},
  [25] = {.lex_state = 14},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [aux_sym__find_token1] = ACTIONS(1),
    [anon_sym_f] = ACTIONS(1),
    [anon_sym_F] = ACTIONS(1),
    [anon_sym_LPAREN] = ACTIONS(1),
    [anon_sym_RPAREN] = ACTIONS(1),
    [aux_sym_date_keyword_token1] = ACTIONS(1),
    [aux_sym_date_keyword_token2] = ACTIONS(1),
    [sym_date_literal] = ACTIONS(1),
    [anon_sym_a] = ACTIONS(1),
    [anon_sym_A] = ACTIONS(1),
    [aux_sym_field_token1] = ACTIONS(1),
    [anon_sym_t] = ACTIONS(1),
    [anon_sym_T] = ACTIONS(1),
    [aux_sym_field_token2] = ACTIONS(1),
    [aux_sym_op_token1] = ACTIONS(1),
    [anon_sym_PLUS] = ACTIONS(1),
    [anon_sym_PIPE] = ACTIONS(1),
    [anon_sym_AMP] = ACTIONS(1),
  },
  [1] = {
    [sym_query] = STATE(24),
    [sym__query] = STATE(20),
    [sym__find] = STATE(6),
    [sym__simple_query] = STATE(13),
    [sym_date_query] = STATE(13),
    [sym_date_keyword] = STATE(23),
    [sym_field] = STATE(18),
    [aux_sym__find_token1] = ACTIONS(3),
    [anon_sym_f] = ACTIONS(5),
    [anon_sym_F] = ACTIONS(5),
    [anon_sym_LPAREN] = ACTIONS(7),
    [aux_sym_date_keyword_token1] = ACTIONS(9),
    [aux_sym_date_keyword_token2] = ACTIONS(9),
    [anon_sym_a] = ACTIONS(11),
    [anon_sym_A] = ACTIONS(11),
    [aux_sym_field_token1] = ACTIONS(13),
    [anon_sym_t] = ACTIONS(11),
    [anon_sym_T] = ACTIONS(11),
    [aux_sym_field_token2] = ACTIONS(13),
  },
  [2] = {
    [sym_query] = STATE(17),
    [sym__query] = STATE(20),
    [sym__find] = STATE(5),
    [sym__simple_query] = STATE(12),
    [sym_date_query] = STATE(12),
    [sym_date_keyword] = STATE(23),
    [sym_field] = STATE(19),
    [aux_sym__find_token1] = ACTIONS(15),
    [anon_sym_f] = ACTIONS(17),
    [anon_sym_F] = ACTIONS(17),
    [anon_sym_LPAREN] = ACTIONS(7),
    [aux_sym_date_keyword_token1] = ACTIONS(9),
    [aux_sym_date_keyword_token2] = ACTIONS(9),
    [anon_sym_a] = ACTIONS(11),
    [anon_sym_A] = ACTIONS(11),
    [aux_sym_field_token1] = ACTIONS(13),
    [anon_sym_t] = ACTIONS(11),
    [anon_sym_T] = ACTIONS(11),
    [aux_sym_field_token2] = ACTIONS(13),
  },
  [3] = {
    [sym_query] = STATE(17),
    [sym__query] = STATE(20),
    [sym__find] = STATE(6),
    [sym__simple_query] = STATE(13),
    [sym_date_query] = STATE(13),
    [sym_date_keyword] = STATE(23),
    [sym_field] = STATE(18),
    [aux_sym__find_token1] = ACTIONS(3),
    [anon_sym_f] = ACTIONS(5),
    [anon_sym_F] = ACTIONS(5),
    [anon_sym_LPAREN] = ACTIONS(7),
    [aux_sym_date_keyword_token1] = ACTIONS(9),
    [aux_sym_date_keyword_token2] = ACTIONS(9),
    [anon_sym_a] = ACTIONS(11),
    [anon_sym_A] = ACTIONS(11),
    [aux_sym_field_token1] = ACTIONS(13),
    [anon_sym_t] = ACTIONS(11),
    [anon_sym_T] = ACTIONS(11),
    [aux_sym_field_token2] = ACTIONS(13),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 8,
    ACTIONS(7), 1,
      anon_sym_LPAREN,
    STATE(19), 1,
      sym_field,
    STATE(22), 1,
      sym__query,
    STATE(23), 1,
      sym_date_keyword,
    ACTIONS(9), 2,
      aux_sym_date_keyword_token1,
      aux_sym_date_keyword_token2,
    ACTIONS(13), 2,
      aux_sym_field_token1,
      aux_sym_field_token2,
    STATE(12), 2,
      sym__simple_query,
      sym_date_query,
    ACTIONS(11), 4,
      anon_sym_a,
      anon_sym_A,
      anon_sym_t,
      anon_sym_T,
  [31] = 8,
    ACTIONS(7), 1,
      anon_sym_LPAREN,
    STATE(16), 1,
      sym__query,
    STATE(19), 1,
      sym_field,
    STATE(23), 1,
      sym_date_keyword,
    ACTIONS(9), 2,
      aux_sym_date_keyword_token1,
      aux_sym_date_keyword_token2,
    ACTIONS(13), 2,
      aux_sym_field_token1,
      aux_sym_field_token2,
    STATE(12), 2,
      sym__simple_query,
      sym_date_query,
    ACTIONS(11), 4,
      anon_sym_a,
      anon_sym_A,
      anon_sym_t,
      anon_sym_T,
  [62] = 8,
    ACTIONS(7), 1,
      anon_sym_LPAREN,
    STATE(16), 1,
      sym__query,
    STATE(18), 1,
      sym_field,
    STATE(23), 1,
      sym_date_keyword,
    ACTIONS(9), 2,
      aux_sym_date_keyword_token1,
      aux_sym_date_keyword_token2,
    ACTIONS(13), 2,
      aux_sym_field_token1,
      aux_sym_field_token2,
    STATE(13), 2,
      sym__simple_query,
      sym_date_query,
    ACTIONS(11), 4,
      anon_sym_a,
      anon_sym_A,
      anon_sym_t,
      anon_sym_T,
  [93] = 2,
    ACTIONS(19), 6,
      aux_sym__find_token1,
      anon_sym_LPAREN,
      aux_sym_date_keyword_token1,
      aux_sym_date_keyword_token2,
      aux_sym_field_token1,
      aux_sym_field_token2,
    ACTIONS(21), 6,
      anon_sym_f,
      anon_sym_F,
      anon_sym_a,
      anon_sym_A,
      anon_sym_t,
      anon_sym_T,
  [110] = 4,
    ACTIONS(27), 1,
      sym_identifier,
    STATE(11), 1,
      aux_sym__term,
    ACTIONS(25), 2,
      aux_sym_op_token1,
      aux_sym_op_token2,
    ACTIONS(23), 4,
      ts_builtin_sym_end,
      anon_sym_PLUS,
      anon_sym_PIPE,
      anon_sym_AMP,
  [127] = 4,
    ACTIONS(33), 1,
      sym_identifier,
    STATE(9), 1,
      aux_sym__term,
    ACTIONS(31), 2,
      aux_sym_op_token1,
      aux_sym_op_token2,
    ACTIONS(29), 4,
      anon_sym_RPAREN,
      anon_sym_PLUS,
      anon_sym_PIPE,
      anon_sym_AMP,
  [144] = 4,
    ACTIONS(36), 1,
      sym_identifier,
    STATE(9), 1,
      aux_sym__term,
    ACTIONS(25), 2,
      aux_sym_op_token1,
      aux_sym_op_token2,
    ACTIONS(23), 4,
      anon_sym_RPAREN,
      anon_sym_PLUS,
      anon_sym_PIPE,
      anon_sym_AMP,
  [161] = 4,
    ACTIONS(38), 1,
      sym_identifier,
    STATE(11), 1,
      aux_sym__term,
    ACTIONS(31), 2,
      aux_sym_op_token1,
      aux_sym_op_token2,
    ACTIONS(29), 4,
      ts_builtin_sym_end,
      anon_sym_PLUS,
      anon_sym_PIPE,
      anon_sym_AMP,
  [178] = 3,
    ACTIONS(41), 1,
      anon_sym_RPAREN,
    STATE(2), 1,
      sym_op,
    ACTIONS(43), 5,
      aux_sym_op_token1,
      aux_sym_op_token2,
      anon_sym_PLUS,
      anon_sym_PIPE,
      anon_sym_AMP,
  [192] = 3,
    ACTIONS(41), 1,
      ts_builtin_sym_end,
    STATE(3), 1,
      sym_op,
    ACTIONS(43), 5,
      aux_sym_op_token1,
      aux_sym_op_token2,
      anon_sym_PLUS,
      anon_sym_PIPE,
      anon_sym_AMP,
  [206] = 1,
    ACTIONS(45), 7,
      ts_builtin_sym_end,
      anon_sym_RPAREN,
      aux_sym_op_token1,
      aux_sym_op_token2,
      anon_sym_PLUS,
      anon_sym_PIPE,
      anon_sym_AMP,
  [216] = 1,
    ACTIONS(47), 7,
      ts_builtin_sym_end,
      anon_sym_RPAREN,
      aux_sym_op_token1,
      aux_sym_op_token2,
      anon_sym_PLUS,
      anon_sym_PIPE,
      anon_sym_AMP,
  [226] = 1,
    ACTIONS(49), 2,
      ts_builtin_sym_end,
      anon_sym_RPAREN,
  [231] = 1,
    ACTIONS(51), 2,
      ts_builtin_sym_end,
      anon_sym_RPAREN,
  [236] = 2,
    ACTIONS(53), 1,
      sym_identifier,
    STATE(8), 1,
      aux_sym__term,
  [243] = 2,
    ACTIONS(55), 1,
      sym_identifier,
    STATE(10), 1,
      aux_sym__term,
  [250] = 1,
    ACTIONS(57), 2,
      ts_builtin_sym_end,
      anon_sym_RPAREN,
  [255] = 1,
    ACTIONS(59), 1,
      sym_date_literal,
  [259] = 1,
    ACTIONS(61), 1,
      anon_sym_RPAREN,
  [263] = 1,
    ACTIONS(63), 1,
      sym_date_literal,
  [267] = 1,
    ACTIONS(65), 1,
      ts_builtin_sym_end,
  [271] = 1,
    ACTIONS(67), 1,
      sym_identifier,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(4)] = 0,
  [SMALL_STATE(5)] = 31,
  [SMALL_STATE(6)] = 62,
  [SMALL_STATE(7)] = 93,
  [SMALL_STATE(8)] = 110,
  [SMALL_STATE(9)] = 127,
  [SMALL_STATE(10)] = 144,
  [SMALL_STATE(11)] = 161,
  [SMALL_STATE(12)] = 178,
  [SMALL_STATE(13)] = 192,
  [SMALL_STATE(14)] = 206,
  [SMALL_STATE(15)] = 216,
  [SMALL_STATE(16)] = 226,
  [SMALL_STATE(17)] = 231,
  [SMALL_STATE(18)] = 236,
  [SMALL_STATE(19)] = 243,
  [SMALL_STATE(20)] = 250,
  [SMALL_STATE(21)] = 255,
  [SMALL_STATE(22)] = 259,
  [SMALL_STATE(23)] = 263,
  [SMALL_STATE(24)] = 267,
  [SMALL_STATE(25)] = 271,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, SHIFT(6),
  [5] = {.entry = {.count = 1, .reusable = false}}, SHIFT(6),
  [7] = {.entry = {.count = 1, .reusable = true}}, SHIFT(4),
  [9] = {.entry = {.count = 1, .reusable = true}}, SHIFT(21),
  [11] = {.entry = {.count = 1, .reusable = false}}, SHIFT(25),
  [13] = {.entry = {.count = 1, .reusable = true}}, SHIFT(25),
  [15] = {.entry = {.count = 1, .reusable = true}}, SHIFT(5),
  [17] = {.entry = {.count = 1, .reusable = false}}, SHIFT(5),
  [19] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_op, 1),
  [21] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_op, 1),
  [23] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__simple_query, 2),
  [25] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__simple_query, 2),
  [27] = {.entry = {.count = 1, .reusable = false}}, SHIFT(11),
  [29] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__term, 2),
  [31] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym__term, 2),
  [33] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__term, 2), SHIFT_REPEAT(9),
  [36] = {.entry = {.count = 1, .reusable = false}}, SHIFT(9),
  [38] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__term, 2), SHIFT_REPEAT(11),
  [41] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__query, 1),
  [43] = {.entry = {.count = 1, .reusable = true}}, SHIFT(7),
  [45] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__simple_query, 3),
  [47] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_date_query, 2),
  [49] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_query, 2),
  [51] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__query, 3),
  [53] = {.entry = {.count = 1, .reusable = true}}, SHIFT(8),
  [55] = {.entry = {.count = 1, .reusable = true}}, SHIFT(10),
  [57] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_query, 1),
  [59] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_date_keyword, 1),
  [61] = {.entry = {.count = 1, .reusable = true}}, SHIFT(14),
  [63] = {.entry = {.count = 1, .reusable = true}}, SHIFT(15),
  [65] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [67] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_field, 1),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_inspire_query(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
    .primary_state_ids = ts_primary_state_ids,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
