module.exports = grammar({
  name: 'inspire_query',

  rules: {
    query: $ => seq(optional($._find), $._query),

    _query: $ => seq($._simple_query, optional(seq($.op, $.query))),

    _find: $ => choice(/[fF][iI][nN][dD]/, "f", "F"),

    _simple_query: $ => choice(
      seq("(", $._query, ")"),
      $.date_query,
      seq($.field, $._term)
    ),

    date_query: $ => seq($.date_keyword, $.date_literal),

    date_keyword: $ => choice(/[dD][eE]/, /[dD][aA][tT][eE]/),

    date_literal: $ => /[0-9]{4}/,

    field: $ => choice("a", "A", /[aA][uU]([tT][hH][oO][rR])?/, "t", "T", /[tT][iI]([tT][lL][eE])?/),

    op: $ => choice(/[oO][rR]/, /[aA][nN][dD]/, "+", "|", "&"),

    _term: $ => repeat1($.identifier),

    identifier: $ => /[a-zA-Z\u{80}-\u{10FFFF}-]+/u,
  }
});
