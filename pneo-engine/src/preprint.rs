use anyhow::Context;

use super::Result;

pub fn id_to_file(id: &str, version: u8) -> String {
    let basename = id.rsplit_once("/").map(|split| split.1).unwrap_or(id);

    format!("{}v{}.pdf", basename, version)
}

/// returns a (basename, version) tuple
pub fn validate<'a>(id: &'a str, referenced_id: &'a str, url: &'a str) -> Result<(&'a str, u8)> {
    const ID_PREFIX: &str = "http://arxiv.org/abs/";

    let identifier = id
        .split_once(ID_PREFIX)
        .filter(|(prefix, _)| *prefix == "")
        .map(|(_, suffix)| suffix)
        .ok_or_else(|| anyhow::anyhow!("unexpected arxiv id {:?}", id))?;

    let (identifier, version) = identifier
        .rsplit_once("v")
        .map(|(prefix, suffix)| (prefix, Some(suffix)))
        .unwrap_or((identifier, None));

    if identifier != referenced_id {
        anyhow::bail!(
            "inconsistent ids. arxiv = {:?} external reference = {:?}",
            identifier,
            referenced_id
        );
    }

    let (basename, url_version) = {
        let index = url
            .rfind("/")
            .filter(|&idx| idx < url.len() - 1)
            .ok_or_else(|| anyhow::anyhow!("unexpected url structure {:?}", url))?;

        let basename = &url[(index + 1)..];

        let version = basename
            .rsplit_once("v")
            .map(|(_, suffix)| suffix)
            .ok_or_else(|| anyhow::anyhow!("no version suffix in {:?}", url))?;

        (basename, version)
    };

    let same_version = match (version, url_version) {
        (Some(version), url_version) => version == url_version,
        (None, _) => true,
    };

    if !same_version {
        anyhow::bail!(
            "inconsistent versions. id = {:?} ({:?}) url = {:?} ({})",
            id,
            version,
            url,
            url_version
        );
    }

    let version = u8::from_str_radix(url_version, 10)
        .with_context(|| format!("invalid version string {:?}", url_version))?;

    Ok((basename, version))
}
