use std::path::{Path, PathBuf};
use std::{
    collections::HashMap,
    io,
    pin::Pin,
    sync::Arc,
    time::{Duration, SystemTime},
};

use anyhow::{Context as _, Result};
use crossterm::event::{
    Event as TerminalEvent, KeyCode, KeyEvent, KeyModifiers, MouseButton, MouseEvent,
    MouseEventKind,
};
use futures::{
    channel::mpsc::{self, UnboundedSender},
    future::{Fuse, FusedFuture},
    stream::FusedStream,
    Future, FutureExt as _, StreamExt as _,
};
use ratatui::backend::Backend;
use ratatui::{layout::Rect, Terminal};
use syntax::Query;

pub mod api;
mod components;
pub mod preprint;
pub mod syntax;
mod ui;

use crate::{
    api::InspireSearch,
    components::{input, spinner},
};

#[derive(Debug, Clone)]
pub struct Article {
    pub control_number: u32,
    pub title: Option<String>,
    pub eprints: Vec<String>,
    pub authors: Vec<String>,
    pub bibtex: String,
    pub created: String,
    pub abstracts: Vec<String>,
}

impl Article {
    fn eprint(&self) -> Option<&str> {
        self.eprints.first().map(|s| &s[..])
    }

    fn abstract_(&self) -> Option<&str> {
        self.abstracts.first().map(|s| &s[..])
    }

    fn authors(&self) -> String {
        let mut s = String::new();

        for author in self.authors.iter() {
            if !s.is_empty() {
                s.push_str(", ");
            }

            s.push_str(author);
        }

        s
    }
}

pub trait Runtime {
    type Cache: Cache + 'static;
    type Store: Store;
    type VersionChecker: VersionChecker;

    fn cache(&self) -> Arc<Self::Cache>;
    fn store(&mut self) -> Option<Self::Store>;
    fn version_checker(&self) -> Self::VersionChecker;

    fn open(&self, uri: impl AsRef<std::ffi::OsStr> + std::fmt::Debug) -> Result<()>;
    fn copy(&self, content: &str) -> Result<()>;

    fn offline(&self) -> bool;

    fn tick(millis: u64) -> impl Future<Output = ()> + 'static;
    fn ticks(millis: u64) -> impl futures::Stream<Item = ()> + 'static {
        futures::stream::repeat(()).then(move |_| Self::tick(millis))
    }

    fn now() -> SystemTime {
        SystemTime::now()
    }

    fn can_stop() -> bool {
        true
    }
}

pub trait VersionChecker {
    fn current(&self) -> &str {
        ""
    }
    fn update(&self) {}

    fn last(&self) -> io::Result<Option<SystemTime>> {
        Ok(None)
    }
}

pub trait Store {
    fn upsert(&mut self, article: &Article) -> Result<()> {
        let _ = article;
        Ok(())
    }

    fn query(&self, query: Query) -> Result<Vec<Article>> {
        let _ = query;
        Ok(vec![])
    }
}

pub trait Cache {
    fn preprint_file_from_id(&self, id: &str) -> Result<Option<PathBuf>>;

    fn has(&self, id: &str, referenced_id: &str, url: &str) -> Result<Option<PathBuf>>;

    fn insert(&self, id: &str, referenced_id: &str, url: &str, content: Vec<u8>)
        -> Result<PathBuf>;

    fn get_downloaded(&self) -> Result<HashMap<String, Download>>;

    fn get_recid(&self, recid: u32) -> Result<Option<PathBuf>>;

    fn insert_recid(&self, recid: u32, file: &std::ffi::OsString) -> anyhow::Result<()>;
}

struct State {
    spinner: spinner::State,
    help: bool,
    output: Option<Result<TableState<Article>>>,
    searching: bool,
    /// (completed, total)
    progress: Option<(usize, usize)>,
    overlay: Option<Overlay>,
    downloaded: HashMap<String, Download>,
    input: input::State,
}

#[derive(Clone, Copy, Debug)]
pub enum Download {
    Arxiv(u8),
    RecId,
}

#[derive(Debug)]
enum Overlay {
    Message { contents: String, error: bool },
    Help,
    Abstract,
}

struct TableState<T> {
    entries: Vec<T>,
    focus: usize,
    page: Option<usize>,
}

impl TableState<Article> {
    fn first_page(result: InspireSearch) -> Self {
        let page = if result.has_next() { Some(0) } else { None };

        Self {
            entries: result.0,
            focus: 0,
            page,
        }
    }
}

impl<T> TableState<T> {
    #[cfg(feature = "local")]
    fn fixed(entries: Vec<T>) -> Self {
        Self {
            entries,
            focus: 0,
            page: None,
        }
    }

    fn down(&mut self, step: usize) -> bool {
        if self.entries.is_empty() {
            return false;
        }

        let next_focus = (self.focus + step).min(self.ui_len() - 1);
        self.change_focus(next_focus)
    }

    fn up(&mut self, step: usize) -> bool {
        if self.entries.is_empty() {
            return false;
        }

        let next_focus = self.focus.saturating_sub(step);
        self.change_focus(next_focus)
    }

    fn ui_len(&self) -> usize {
        if self.page.is_some() {
            self.entries.len() + 1
        } else {
            self.entries.len()
        }
    }

    /// (offset, marker)
    fn draw(&mut self, size: usize, offset_state: &mut usize) -> (usize, usize) {
        let space = size as isize;
        let mut offset = *offset_state as isize;
        let mut marker = self.focus as isize - offset;
        let entries = self.ui_len() as isize;

        log::trace!(
            "space/entries = {}/{} marker = {} offset = {}",
            space,
            entries,
            marker,
            offset
        );

        if (marker + 1) > space {
            offset += marker + 1 - space;
            marker = space - 1;
        } else if marker < 0 {
            offset += marker;
            marker = 0;
        }

        let showed = (entries - offset).min(space);

        if showed < entries && showed < space {
            let diff = space - showed;
            let next_offset = (offset - diff).min(0);
            marker += offset - next_offset;
            offset = next_offset;
        }

        log::trace!(
            "space/entries = {}/{} marker = {} offset = {}",
            space,
            entries,
            marker,
            offset
        );

        *offset_state = offset as usize;

        (offset as usize, marker as usize)
    }

    fn change_focus(&mut self, focus: usize) -> bool {
        if focus == self.focus {
            return false;
        }

        self.focus = focus;
        true
    }

    fn set(&mut self, row: usize) -> bool {
        if self.entries.is_empty() {
            return false;
        }

        self.change_focus(row)
    }
}

struct ExtraState {
    table: TableRender,
    overlay: Rect,
    last_click: (SystemTime, usize),
    input: input::ExtraState,
}

impl ExtraState {
    fn reset_table_offset(&mut self) {
        self.table.offset = 0;
    }
}

impl Overlay {
    fn info(contents: String) -> Self {
        Self::Message {
            contents,
            error: false,
        }
    }
}

struct TableRender {
    size: Rect,
    offset: usize,
}

impl State {
    fn offline(&self) -> bool {
        self.input.offline()
    }

    fn warn(&mut self, error: Option<String>) {
        if let Some(contents) = error {
            self.overlay = Some(Overlay::Message {
                error: true,
                contents,
            });
        }
    }

    fn main_focus(&self) -> bool {
        self.overlay.is_none()
    }

    fn table_focus(&self) -> bool {
        self.main_focus() || std::matches![self.overlay, Some(Overlay::Abstract)]
    }

    fn has_overlay(&self) -> bool {
        self.overlay.is_some()
    }

    fn busy(&self) -> bool {
        self.searching || self.blocked()
    }

    fn set_searching<R: Runtime>(&mut self, searching: bool) {
        self.searching = searching;
        self.spinner.toggle::<R>(self.busy())
    }

    fn set_progress<R: Runtime>(&mut self, progress: Option<(usize, usize)>) {
        self.progress = progress;
        self.spinner.toggle::<R>(self.busy())
    }

    fn blocked(&self) -> bool {
        self.progress.is_some()
    }

    fn down(&mut self, step: usize) -> bool {
        if self.busy() {
            return false;
        }

        let Some(Ok(table)) = &mut self.output else {
            return false;
        };

        table.down(step)
    }

    fn up(&mut self, step: usize) -> bool {
        if self.busy() {
            return false;
        }

        let Some(Ok(table)) = &mut self.output else {
            return false;
        };

        table.up(step)
    }

    fn click_entry(&mut self, row: usize) -> bool {
        if self.busy() {
            return false;
        }

        let Some(Ok(table)) = &mut self.output else {
            return false;
        };

        table.set(row)
    }

    fn update_downloaded(&mut self, cache: &impl Cache) -> bool {
        let update = cache.get_downloaded().map_err(|e| format!("{:?}", e));

        match update {
            Ok(downloaded) => self.downloaded = downloaded,
            Err(error) => {
                self.warn(Some(error));
                return false;
            }
        }

        true
    }
}

#[derive(Debug)]
enum Event {
    PneoUpdate(Result<Option<String>>),
    Terminal(Option<io::Result<TerminalEvent>>),
    SearchResponse(Result<(InspireSearch, usize)>),
    BibtexResponse(anyhow::Result<String>),
    Preprint(Result<(PathBuf, String, bool)>),
    DownloadProgress((usize, usize)),
    Spin,
    Commit,
    PreprintUpdate(Result<Option<String>>),
}

pub trait AsMutTerminal<B: Backend> {
    fn as_mut_terminal(&mut self) -> &mut Terminal<B>;
}

impl<B: Backend> AsMutTerminal<B> for Terminal<B> {
    fn as_mut_terminal(&mut self) -> &mut Terminal<B> {
        self
    }
}

impl<B: Backend> AsMutTerminal<B> for &mut Terminal<B> {
    fn as_mut_terminal(&mut self) -> &mut Terminal<B> {
        self
    }
}

pub async fn main_loop<B: ratatui::backend::Backend, RT: Runtime>(
    mut terminal: impl AsMutTerminal<B>,
    mut runtime: RT,
    terminal_events: impl futures::Stream<Item = std::io::Result<TerminalEvent>> + Unpin,
) -> anyhow::Result<()> {
    let cache = runtime.cache();

    let mut store = runtime
        .store()
        .ok_or_else(|| anyhow::anyhow!("unexpected missing store"))?;

    #[cfg(feature = "local")]
    let parser = {
        let language = tree_sitter_inspire_query::language();

        let mut parser = tree_sitter::Parser::new();
        parser.set_language(language).unwrap();

        parser
    };

    let mut state = State {
        help: true,
        spinner: spinner::State::new(),
        output: None,
        searching: false,
        progress: None,
        overlay: None,
        downloaded: cache.get_downloaded()?,
        #[cfg(feature = "local")]
        input: input::State::new(parser, runtime.offline()),
        #[cfg(not(feature = "local"))]
        input: input::State::new(runtime.offline()),
    };

    let mut extra_state = ExtraState {
        last_click: (std::time::UNIX_EPOCH, usize::MAX),
        table: TableRender {
            size: Default::default(),
            offset: 0,
        },
        overlay: Default::default(),
        input: input::ExtraState::new(),
    };

    let mut draw = true;
    let mut redraw = false;

    let (downloader, mut download_progress) = Downloader::new(cache.clone());
    let search_request = Fuse::terminated();
    let preprint_request = Fuse::terminated();
    let preprint_update_request = Fuse::terminated();
    let bibtex_request = Fuse::terminated();
    let mut terminal_events = terminal_events.fuse();

    #[cfg(all(not(feature = "deb"), feature = "local"))]
    let pneo_version_check =
        version::pneo_background_version_check(runtime.version_checker()).fuse();

    #[cfg(any(feature = "deb", not(feature = "local")))]
    let pneo_version_check = futures::future::pending::<Result<_>>().fuse();

    futures::pin_mut!(search_request);
    futures::pin_mut!(preprint_request);
    futures::pin_mut!(preprint_update_request);
    futures::pin_mut!(bibtex_request);
    futures::pin_mut!(pneo_version_check);

    'main_loop: loop {
        if draw {
            log::trace!("drawing!");

            ui::ui(
                terminal.as_mut_terminal(),
                &mut state,
                &mut extra_state,
                redraw,
            )?;
        }

        draw = true;
        redraw = false;

        log::trace!("next message...");
        let event = (async {
            let mut spin = state.spinner.stream();
            let mut throttle = state.input.throttle();

            futures::select! {
                ev =  terminal_events.next() => Event::Terminal(ev),
                res =  &mut search_request => Event::SearchResponse(res),
                res =  &mut bibtex_request => Event::BibtexResponse(res),
                _ =  spin.next() => Event::Spin,
                _ =  &mut throttle => Event::Commit,
                preprint = &mut preprint_request => Event::Preprint(preprint),
                preprint_update = &mut preprint_update_request => Event::PreprintUpdate(preprint_update),
                progress = download_progress.next() => Event::DownloadProgress(progress.unwrap_or((0, 1))),
                ver = &mut pneo_version_check => Event::PneoUpdate(ver),
            }
        }).await;

        log::trace!("message = {event:?}");

        match event {
            Event::Terminal(None) => return Ok(()),
            Event::Terminal(Some(ev)) => {
                let handled = handle_terminal_event::<RT>(&mut state, ev?, &mut extra_state);

                draw = handled.draw;
                redraw = handled.clear;

                if redraw {
                    state.update_downloaded(&*cache);
                }

                let Some(action) = handled.action else {
                    continue;
                };

                state.help = false;

                match action {
                    Action::Quit => {
                        if RT::can_stop() {
                            return Ok(());
                        }
                    }
                    Action::Cancel => {
                        preprint_request.set(Fuse::terminated());
                        state.set_progress::<RT>(None);
                    }
                    Action::Input => {
                        search_request.set(Fuse::terminated());
                        state.set_searching::<RT>(true);
                    }
                    Action::Select => {
                        let Some(table) = state.output.as_ref().and_then(|res| res.as_ref().ok())
                        else {
                            draw = false;
                            continue;
                        };

                        let selecting_more = table.focus == table.ui_len() - 1;

                        if let Some(page) = table.page.filter(|_| selecting_more) {
                            log::info!("requesting with {:?}", &state.input.value());
                            state.set_searching::<RT>(true);
                            search_request.set(search(state.input.value().to_string(), page + 1).fuse());
                            continue;
                        }

                        let entry = table.entries.get(table.focus as usize);

                        let Some(entry) = entry else {
                            log::warn!("Unexpected missing table focus");
                            state.warn(Some(format!("Unexpected error")));
                            continue;
                        };

                        // remove overlay
                        let _ = state.overlay.take();

                        let preprint_id = {
                            if entry.eprints.len() > 1 {
                                log::warn!("multiple eprints for {:?}", entry);
                            }

                            entry.eprint()
                        };

                        'arxiv: {
                            let Some(preprint_id) = preprint_id else {
                                break 'arxiv;
                            };

                            match cache.preprint_file_from_id(preprint_id) {
                                Err(err) => {
                                    state.warn(Some(format!("{}", err)));
                                }
                                Ok(Some(filename)) => {
                                    // NOTE: this will override any ongoing update check, which might be... OK?
                                    if !state.offline() {
                                        preprint_update_request
                                            .set(downloader.download_check(preprint_id.to_string()).fuse());
                                    }

                                    state.warn(
                                        runtime.open(Path::new(&filename))
                                            .err()
                                            .map(|err| format!("{:?}", err)),
                                    );
                                }
                                Ok(None) if !state.offline() => {
                                    let preprint_id = preprint_id.to_string();
                                    state.set_progress::<RT>(Some((0, 0)));
                                    preprint_request.set(downloader.download(preprint_id).fuse());
                                }
                                Ok(None) => {}
                            }

                            continue 'main_loop;
                        }

                        match cache.get_recid(entry.control_number) {
                            Ok(Some(filename)) => {
                                    state.warn(
                                        runtime.open(Path::new(&filename))
                                            .err()
                                            .map(|err| format!("{:?}", err)),
                                    );
                            },
                            Ok(None) => {
                                state.warn(Some(format!("INSPIRE record {} is not available.\n\nStore it in pneo's cache by running:\n\n  pneo record {} <FILE>", entry.control_number, entry.control_number)));
                            },
                            Err(err) => state.warn(Some(format!("{}", err))),
                        }
                    }
                    Action::Copy => {
                        let Some(table) = state.output.as_ref().and_then(|res| res.as_ref().ok())
                        else {
                            draw = false;
                            continue;
                        };

                        let entry = table.entries.get(table.focus as usize);

                        let Some(entry) = entry else {
                            log::warn!("Unexpected missing table focus");
                            state.warn(Some(format!("Unexpected error")));
                            continue;
                        };

                        let url = entry.bibtex.clone();
                        state.set_progress::<RT>(Some((0, 0)));
                        bibtex_request.set(api::get_bibtext(url).fuse());
                    }
                    Action::ToggleOffline if !cfg!(feature = "local") => {}
                    Action::ToggleOffline if state.offline() => {
                        state.input.set_offline::<RT>(false);

                        state.set_searching::<RT>(true);
                    }
                    Action::ToggleOffline /* if !state.offline */ => {
                        state.input.set_offline::<RT>(true);

                        state.overlay = None;
                        search_request.set(Fuse::terminated());
                        bibtex_request.set(Fuse::terminated());
                        preprint_update_request.set(Fuse::terminated());
                        preprint_request.set(Fuse::terminated());

                        #[cfg(feature = "local")]
                        let _ = local_query(&mut state, &mut extra_state, &store);
                    }
                    Action::ToggleAbstract => {
                        state.overlay = match state.overlay {
                            Some(Overlay::Abstract) => None,
                            None => Some(Overlay::Abstract),
                            overlay => overlay
                        };
                    },
                    Action::ToggleHelp if state.overlay.is_none() => {
                        state.overlay = Some(Overlay::Help);
                    }
                    Action::ToggleHelp /* if state.overlay.is_some() */ => {}
                    Action::OpenExternally => {
                        let Some(table) = state.output.as_ref().and_then(|res| res.as_ref().ok())
                        else {
                            draw = false;
                            continue;
                        };

                        let entry = table.entries.get(table.focus as usize);

                        let Some(entry) = entry else {
                            log::warn!("Unexpected missing table focus");
                            state.warn(Some(format!("Unexpected error")));
                            continue;
                        };

                        state.warn(runtime.open(format!("https://inspirehep.net/literature/{}", entry.control_number)).err().map(|err| format!("{:?}", err)))
                    }
                }
            }
            Event::SearchResponse(res) => {
                log::debug!("search response {:?}", res);

                let mut reset = false;

                let output = match (state.output, res) {
                    (None | Some(Err(_)), res) => res.map(|(articles, _)| {
                        reset = true;
                        TableState::first_page(articles)
                    }),
                    (_, Err(err)) => Err(err),
                    (Some(Ok(mut table)), Ok((articles, page))) => {
                        if table.page.map(|p| p + 1) == Some(page) {
                            table.page = table.page.map(|p| p + 1).filter(|_| articles.has_next());
                            table.entries.extend(articles.0);
                            if table.page.is_none() && table.focus as usize == table.entries.len() {
                                table.focus -= 1;
                            }
                            Ok(table)
                        } else {
                            reset = true;
                            Ok(TableState::first_page(articles))
                        }
                    }
                };

                state.output = Some(output);

                if reset {
                    extra_state.reset_table_offset();
                }

                state.set_searching::<RT>(false);
            }
            Event::Spin => {
                state.spinner.tick();
            }
            Event::Commit => {
                if state.input.value().len() < 3 {
                    state.set_searching::<RT>(false);
                    continue;
                }

                if !state.offline() {
                    log::info!("requesting with {:?}", &state.input.value());
                    search_request.set(search(state.input.value().to_string(), 0).fuse());
                    continue;
                }

                state.set_searching::<RT>(false);

                #[cfg(feature = "local")]
                if !local_query(&mut state, &mut extra_state, &store) {
                    continue;
                }
            }
            Event::Preprint(res) => {
                state.set_progress::<RT>(None);

                let (file_path, eprint, _cached) = match res {
                    Err(err) => {
                        log::error!("{:?}", err);
                        state.warn(Some(format!("{}", err)));
                        continue;
                    }
                    Ok(res) => res,
                };

                if !state.update_downloaded(&*cache) {
                    continue;
                }

                let record = state
                    .output
                    .as_ref()
                    .and_then(|res| res.as_ref().ok())
                    .into_iter()
                    .flat_map(|table| table.entries.iter())
                    .find(|entry| entry.eprint() == Some(&eprint));

                if let Err(err) = record.map(|article| store.upsert(article)).transpose() {
                    log::error!("error while adding record: {:?}", err);
                }

                state.warn(
                    runtime
                        .open(&file_path)
                        .err()
                        .map(|err| format!("{:?}", err)),
                );
            }
            Event::DownloadProgress(progress) => {
                state.set_progress::<RT>(state.progress.and(Some(progress)));
            }
            Event::BibtexResponse(res) => {
                state.set_progress::<RT>(None);

                let bibtex = match res {
                    Ok(bibtex) => bibtex,
                    Err(err) => {
                        log::error!("{:?}", err);
                        state.warn(Some(format!("{}", err)));
                        continue;
                    }
                };

                state.warn(runtime.copy(&bibtex).err().map(|err| format!("{:?}", err)));

                if state.overlay.is_none() {
                    state.overlay = Some(Overlay::info(format!("bibtex copied")));
                }
            }
            Event::PneoUpdate(check) => {
                let version = match check {
                    Ok(Some(version)) => version,
                    Ok(None) => {
                        continue;
                    }
                    Err(err) => {
                        log::error!("error version checking: {:?}", err);
                        continue;
                    }
                };

                if state.overlay.is_none() {
                    let message = format!("New version available: {version}.");
                    state.overlay = Some(Overlay::info(message));
                    #[cfg(not(feature = "deb"))]
                    runtime.version_checker().update();
                }
            }
            Event::PreprintUpdate(res) => {
                let eprint = match res {
                    Ok(Some(eprint)) => eprint,
                    Ok(None) => {
                        continue;
                    }
                    Err(err) => {
                        log::error!("error updating preprint: {:?}", err);
                        state.warn(Some(format!("Error upgrading to new version\n{}", err)));
                        continue;
                    }
                };

                if state.main_focus() && !state.busy() {
                    state.overlay = state.overlay.or_else(|| {
                        Some(Overlay::info(format!(
                            "New version of {} available. Downloading...",
                            eprint
                        )))
                    });

                    debug_assert!(preprint_request.is_terminated());

                    state.set_progress::<RT>(Some((0, 0)));
                    preprint_request.set(downloader.download(eprint).fuse());
                }
            }
        }
    }
}

#[derive(Debug)]
enum Action {
    Input,
    Select,
    Copy,
    Quit,
    Cancel,
    ToggleOffline,
    ToggleAbstract,
    ToggleHelp,
    OpenExternally,
}

struct HandledEvent {
    draw: bool,
    clear: bool,
    action: Option<Action>,
}

fn handle_terminal_event<R: Runtime>(
    state: &mut State,
    ev: TerminalEvent,
    extra_state: &mut ExtraState,
) -> HandledEvent {
    let mut handled = HandledEvent {
        draw: false,
        clear: false,
        action: None,
    };

    handled.action = match ev {
        TerminalEvent::Resize(..) => {
            handled.draw = true;
            return handled;
        }
        TerminalEvent::Mouse(mouse) if state.table_focus() => match mouse.kind {
            MouseEventKind::ScrollDown => {
                if state.down(1) {
                    None
                } else {
                    return handled;
                }
            }
            MouseEventKind::ScrollUp => {
                if state.up(1) {
                    None
                } else {
                    return handled;
                }
            }
            MouseEventKind::Up(MouseButton::Left) => {
                let MouseEvent { column, row, .. } = mouse;

                let click = Rect {
                    x: column,
                    y: row,
                    width: 1,
                    height: 1,
                };

                if !extra_state.table.size.intersects(click) {
                    return handled;
                }

                let has_overlay = state.has_overlay();

                if has_overlay && extra_state.overlay.intersects(click) {
                    return handled;
                }

                let index =
                    click.y as usize - extra_state.table.size.y as usize + extra_state.table.offset;

                let changed = state.click_entry(index);

                let clicked = state
                    .output
                    .as_ref()
                    .map(|res| res.as_ref().ok())
                    .flatten()
                    .map(|table| table.focus == index)
                    .unwrap_or(false);

                let in_window = R::now()
                    .duration_since(extra_state.last_click.0)
                    .ok()
                    .map(|dur| dur < Duration::from_millis(500))
                    .unwrap_or(true);

                let same_row = extra_state.last_click.1 == index;

                if clicked && !has_overlay {
                    extra_state.last_click = (R::now(), index);
                }

                if in_window && same_row && !has_overlay {
                    Some(Action::Select)
                } else if changed {
                    None
                } else {
                    return handled;
                }
            }
            _ => return handled,
        },
        TerminalEvent::Key(KeyEvent {
            code: KeyCode::Char('r'),
            modifiers,
            ..
        }) if modifiers == KeyModifiers::CONTROL => {
            handled.draw = true;
            handled.clear = true;
            return handled;
        }
        TerminalEvent::Key(key) if state.table_focus() => match key.code {
            KeyCode::Esc if state.main_focus() => {
                if state.blocked() {
                    Some(Action::Cancel)
                } else {
                    Some(Action::Quit)
                }
            }
            KeyCode::Esc => {
                let _ = state.overlay.take();
                None
            }
            KeyCode::Char(ch) => {
                if state.blocked() {
                    return handled;
                }

                let control = key.modifiers == KeyModifiers::CONTROL;

                // searching b/c blocked() is false
                let searching = state.busy();
                let offline = state.offline();

                match (control, ch) {
                    (true, 'b') if state.main_focus() && !(searching || offline) => {
                        Some(Action::Copy)
                    }
                    (true, 'o') if state.main_focus() && !searching => Some(Action::ToggleOffline),
                    (true, 'a') if !(searching || offline) => Some(Action::ToggleAbstract),
                    (true, 'p') if !searching => Some(Action::OpenExternally),
                    (true, 'h') if state.main_focus() && !searching => Some(Action::ToggleHelp),
                    (true, 'w') if state.main_focus() => {
                        state.input.delete_word::<R>();
                        Some(Action::Input)
                    }
                    (true, _) => return handled,
                    (false, ch) if state.main_focus() => {
                        state.input.append::<R>(ch);
                        Some(Action::Input)
                    }
                    _ => return handled,
                }
            }
            KeyCode::Enter => {
                if state.busy() {
                    return handled;
                } else if state
                    .output
                    .as_ref()
                    .and_then(|res| res.as_ref().ok())
                    .map(|table| !table.entries.is_empty())
                    .unwrap_or(false)
                {
                    Some(Action::Select)
                } else {
                    return handled;
                }
            }
            KeyCode::Delete | KeyCode::Backspace if state.main_focus() => {
                if !state.blocked() && state.input.delete::<R>(key.code == KeyCode::Backspace) {
                    Some(Action::Input)
                } else {
                    return handled;
                }
            }
            key @ (KeyCode::Down | KeyCode::PageDown) => {
                let step = if key == KeyCode::Down { 1 } else { 10 };

                if state.down(step) {
                    None
                } else {
                    return handled;
                }
            }
            key @ (KeyCode::Up | KeyCode::PageUp) => {
                let step = if key == KeyCode::Up { 1 } else { 10 };
                if state.up(step) {
                    None
                } else {
                    return handled;
                }
            }
            dir @ (KeyCode::Left | KeyCode::Right) if state.main_focus() => {
                let step = if dir == KeyCode::Left { -1 } else { 1 };

                if key.modifiers == KeyModifiers::CONTROL {
                    state.input.move_cursor_word(step);
                } else {
                    state.input.move_cursor(step);
                }

                None
            }
            _ => return handled,
        },
        TerminalEvent::Key(key) if state.has_overlay() => match key.code {
            KeyCode::Esc => {
                let _ = state.overlay.take();
                None
            }
            _ => return handled,
        },
        _ => return handled,
    };

    handled.draw = true;

    return handled;
}

struct Downloader {
    tx: UnboundedSender<(usize, usize)>,
    cache: Arc<dyn Cache>,
}

impl Downloader {
    fn new(cache: Arc<impl Cache + 'static>) -> (Self, impl FusedStream<Item = (usize, usize)>) {
        let (tx, rx) = mpsc::unbounded();

        (Self { tx, cache }, rx)
    }

    async fn download_metadata(
        &self,
        preprint_id: impl AsRef<str>,
    ) -> Result<(String, String, Option<PathBuf>)> {
        log::info!("requesting preprint {}", preprint_id.as_ref());

        let mut preprint = api::get_preprint(preprint_id.as_ref())
            .await
            .context("error downloading preprint info")?;

        let entry = if preprint.entry.len() == 1 {
            preprint.entry.pop().unwrap()
        } else {
            anyhow::bail!("invalid preprint response {:?}", preprint)
        };

        let link = entry
            .link
            .into_iter()
            .filter(|link| link.title.as_deref() == Some("pdf"))
            .next();

        let Some(link) = link else {
            anyhow::bail!("unable to find pdf link");
        };

        let url = {
            let href = link.href;

            if href.starts_with("http://") {
                href.replace("http://", "https://")
            } else {
                href
            }
        };

        let cached = self
            .cache
            .has(&entry.id, preprint_id.as_ref(), &url)
            .context("error checking preprint")?;

        return Ok((entry.id, url, cached));
    }

    async fn download_check(&self, preprint_id: String) -> Result<Option<String>> {
        let (_, _, cached) = self.download_metadata(&preprint_id).await?;

        Ok(Some(preprint_id).filter(|_| cached.is_none()))
    }

    // (file_path, preprint_id, cached)
    async fn download(&self, preprint_id: String) -> Result<(PathBuf, String, bool)> {
        let (entry_id, url, cached) = self.download_metadata(&preprint_id).await?;

        if let Some(cached) = cached {
            log::info!("found version in cache, skipping download");
            return Ok((cached, preprint_id, true));
        }

        log::info!("downloading {}", url);

        let response = reqwest::get(&url).await.context("error downloading pdf")?;

        let len = response
            .headers()
            .get(reqwest::header::CONTENT_LENGTH)
            .and_then(|value| value.to_str().ok())
            .and_then(|val| usize::from_str_radix(val, 10).ok());

        let bytes = match len {
            Some(len) if len > 0 => {
                let _ = self.tx.unbounded_send((0, len));
                let mut bytes = Vec::with_capacity(len);
                let mut body = response.bytes_stream();

                loop {
                    let Some(chunk) = body.next().await else {
                        break;
                    };

                    let chunk = chunk?;

                    if chunk.is_empty() {
                        break;
                    }
                    bytes.extend_from_slice(&*chunk);
                    let _ = self.tx.unbounded_send((bytes.len(), len));
                }

                bytes
            }
            _ => response.bytes().await?.into(),
        };

        let path = self
            .cache
            .insert(&entry_id, &preprint_id, &url, bytes)
            .context("error saving preprint")?;

        return Ok((path, preprint_id, false));
    }
}

#[cfg(feature = "local")]
fn local_query(state: &mut State, extra: &mut ExtraState, store: &impl Store) -> bool {
    let query = match state.input.query() {
        Ok(query) => query,
        Err(err) => {
            log::error!("Invalid query {:?}", err);

            return false;
        }
    };

    state.output = Some(store.query(query).map(TableState::fixed));
    extra.reset_table_offset();

    true
}

async fn search(input: String, page: usize) -> Result<(InspireSearch, usize)> {
    let res = api::search_inspire(input, page).await?;

    Ok((res, page))
}

type DynFuture<T> = Pin<Box<dyn FusedFuture<Output = T>>>;
type DynStream<T> = Pin<Box<dyn FusedStream<Item = T>>>;

#[cfg(all(feature = "local", not(feature = "deb")))]
mod version {
    use anyhow::Result;

    use crate::VersionChecker;

    pub async fn pneo_background_version_check(
        checker: impl VersionChecker,
    ) -> Result<Option<String>> {
        use std::time;

        // let metadata = match checker.last() {
        //     Ok(met) => Some(met),
        //     Err(err) if err.kind() == std::io::ErrorKind::NotFound => {
        //         checker.update();
        //         None
        //     }
        //     Err(err) => Err(err)?,
        // };

        // let last_check = metadata
        //     // should only error in weird platforms
        //     .and_then(|met| met.modified().ok())
        //     .unwrap_or(time::UNIX_EPOCH);

        let last_check = checker.last()?.unwrap_or(time::UNIX_EPOCH);

        const CHECK_INTERVAL: time::Duration = time::Duration::from_secs(86400);

        match time::SystemTime::now()
            .duration_since(last_check)
            .map(|dur| dur > CHECK_INTERVAL)
        {
            Ok(true) => {}
            _ => {
                return Ok(None);
            }
        };

        pneo_version_check(checker.current()).await
    }

    async fn pneo_version_check(raw_current_version: &str) -> Result<Option<String>> {
        let response =
            reqwest::get("https://gitlab.com/api/v4/projects/45570088/releases?per_page=1").await?;

        #[derive(serde::Deserialize)]
        struct Release {
            name: String,
        }

        let mut releases: Vec<Release> =
            serde_json::from_str(&response.text().await.map_err(|err| anyhow::anyhow!(err))?)?;

        let remote_version_str = releases
            .pop()
            .ok_or_else(|| anyhow::anyhow!("no releases"))?
            .name;

        let remote_version = semver::Version::parse(&remote_version_str)?;
        let mainline = remote_version.pre.is_empty() && remote_version.build.is_empty();

        let current_version = semver::Version::parse(raw_current_version)?;

        if current_version >= remote_version || !mainline {
            return Ok(None);
        }

        Ok(Some(remote_version_str))
    }
}
