use once_cell::sync::Lazy;
use serde::Deserialize;

use crate::Article;

static CLIENT: Lazy<reqwest::Client> = Lazy::new(|| get_client());

const PAGE_SIZE: usize = 50;
const PAGE_SIZE_PARAM: &str = "50";

#[derive(Deserialize, Debug)]
struct InspireSearchResult {
    hits: Hits,
}

#[derive(Deserialize, Debug)]
struct Hits {
    pub hits: Vec<NestedHit>,
}

#[derive(Deserialize, Debug)]
struct NestedHit {
    pub created: String,
    pub links: Links,
    pub metadata: Metadata,
}

impl TryFrom<NestedHit> for Article {
    fn try_from(hit: NestedHit) -> Result<Article, ()> {
        let created = hit.created_date().map(|cr| cr.to_string()).ok_or_else(|| {
            log::error!(
                "article without date {:?}, date: {:?}",
                hit.metadata.control_number,
                hit.created
            );
            ()
        })?;

        Ok(Self {
            control_number: hit.metadata.control_number,
            title: hit.metadata.titles.into_iter().map(|ti| ti.title).next(),
            eprints: hit
                .metadata
                .arxiv_eprints
                .into_iter()
                .map(|ep| ep.value)
                .collect(),
            authors: hit
                .metadata
                .authors
                .into_iter()
                .filter_map(|au| au.last_name)
                .collect(),
            bibtex: hit.links.bibtex,
            abstracts: hit
                .metadata
                .abstracts
                .into_iter()
                .map(|abs| abs.value)
                .collect(),
            created,
        })
    }

    type Error = ();
}

impl NestedHit {
    pub fn created_date(&self) -> Option<&str> {
        self.created.split_once("T").map(|split| split.0)
    }
}

#[derive(Deserialize, Debug)]
struct Links {
    pub bibtex: String,
}

#[derive(Deserialize, Debug)]
struct Metadata {
    pub control_number: u32,

    pub titles: Vec<Title>,

    #[serde(default)]
    pub arxiv_eprints: Vec<ArxivEprint>,

    #[serde(default)]
    pub authors: Vec<Author>,

    #[serde(default)]
    pub abstracts: Vec<Abstract>,
}

#[derive(Deserialize, Debug)]
struct Author {
    pub last_name: Option<String>,
}

#[derive(Deserialize, Debug)]
struct ArxivEprint {
    pub value: String,
}

#[derive(Deserialize, Debug)]
struct Title {
    pub title: String,
}

#[derive(Deserialize, Debug)]
struct Abstract {
    pub value: String,
}

#[derive(Debug)]
pub struct InspireSearch(pub Vec<Article>);

impl InspireSearch {
    pub fn has_next(&self) -> bool {
        self.0.len() >= PAGE_SIZE
    }
}

pub async fn search_inspire(input: String, page: usize) -> anyhow::Result<InspireSearch> {
    let url = reqwest::Url::parse_with_params("https://inspirehep.net/api/literature", [
        ("q", &input[..]),
        ("sort", "mostrecent"),
        ("size", PAGE_SIZE_PARAM),
        ("fields", "titles.title,arxiv_eprints.value,authors.last_name,abstracts.value,dois.value,dois.material"),
        ("page", &format!("{}", page + 1)),
    ])?;

    let response = CLIENT.get(url).send().await?;

    let inspire_search_result = response.json::<InspireSearchResult>().await?;

    let articles = inspire_search_result
        .hits
        .hits
        .into_iter()
        .filter_map(|hit| Article::try_from(hit).ok())
        .collect();

    Ok(InspireSearch(articles))
}

#[derive(Deserialize, Debug)]
pub struct ArxivSearchResult {
    pub entry: Vec<ArxivEntry>,
}

#[derive(Deserialize, Debug)]
pub struct ArxivEntry {
    pub id: String,
    pub link: Vec<Link>,
}

#[derive(Deserialize, Debug)]
pub struct Link {
    pub title: Option<String>,
    pub href: String,
}

pub async fn get_preprint(id: &str) -> anyhow::Result<ArxivSearchResult> {
    let url =
        reqwest::Url::parse_with_params("https://export.arxiv.org/api/query", [("id_list", id)])?;

    let response = CLIENT.get(url).send().await?;

    let body = response.text().await?;
    Ok(quick_xml::de::from_str(&body)?)
}

fn get_client() -> reqwest::Client {
    #[cfg(not(target_arch = "wasm32"))]
    {
        reqwest::ClientBuilder::new()
            .timeout(std::time::Duration::from_secs(20))
            .build()
            .unwrap()
    }

    #[cfg(target_arch = "wasm32")]
    reqwest::Client::new()
}

pub async fn get_bibtext(url: String) -> anyhow::Result<String> {
    let res = reqwest::get(url).await?;

    Ok(res.text().await?)
}
