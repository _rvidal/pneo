pub mod input {
    use futures::{future::FusedFuture, FutureExt as _};
    use ratatui::{
        layout::Rect,
        style::{Color, Style},
        text::Span,
        widgets::Block,
        Frame,
    };
    #[cfg(feature = "local")]
    use tree_sitter::{Parser, Tree};
    #[cfg(feature = "local")]
    use tree_sitter_highlight::{HighlightConfiguration, HighlightEvent, Highlighter};
    use unicode_width::UnicodeWidthChar;

    #[cfg(feature = "local")]
    use crate::syntax::tree_to_query;
    use crate::{DynFuture, Runtime};

    pub struct State {
        input: String,
        cursor: usize,
        throttle: DynFuture<()>,
        #[cfg(feature = "local")]
        tree: Tree,
        #[cfg(feature = "local")]
        parser: Parser,
        offline: bool,
    }

    pub struct ExtraState {
        #[cfg(feature = "local")]
        highlighter: Highlighter,
        #[cfg(feature = "local")]
        highlighter_conf: HighlightConfiguration,
    }

    impl ExtraState {
        #[cfg(not(feature = "local"))]
        pub fn new() -> Self {
            Self {}
        }

        #[cfg(feature = "local")]
        pub fn new() -> Self {
            const QUERY: &str = r#"(op) @operand
[(date_keyword) (field)] @filter
[(identifier) (date_literal)] @term
"#;

            let highlighter = Highlighter::new();
            let conf = {
                let mut conf = HighlightConfiguration::new(
                    tree_sitter_inspire_query::language(),
                    QUERY,
                    "",
                    "",
                )
                .expect("unable to set highlight config");

                conf.configure(&["operand", "filter", "term"]);
                conf
            };

            Self {
                highlighter,
                highlighter_conf: conf,
            }
        }
    }

    impl State {
        #[cfg(feature = "local")]
        pub fn new(mut parser: Parser, offline: bool) -> Self {
            let tree = parser.parse("", None).unwrap();

            Self {
                input: String::new(),
                cursor: 0,
                throttle: Box::pin(futures::future::pending()),
                parser,
                tree,
                offline,
            }
        }

        #[cfg(not(feature = "local"))]
        pub fn new(offline: bool) -> Self {
            Self {
                input: String::new(),
                cursor: 0,
                throttle: Box::pin(futures::future::pending()),
                offline,
            }
        }

        pub fn value(&self) -> &str {
            &self.input
        }

        #[cfg(feature = "local")]
        pub fn query(&self) -> anyhow::Result<crate::syntax::Query> {
            tree_to_query(&self.tree, &self.input)
        }

        pub fn offline(&self) -> bool {
            self.offline
        }

        pub fn throttle(&mut self) -> impl FusedFuture + '_ {
            &mut self.throttle
        }

        fn throttle_on_input<R: Runtime>(&mut self) {
            const DEFAULT_THROTTLE: u64 = 400;
            const OFFLINE_THROTTLE: u64 = 100;

            let throttle_duration = if self.offline {
                OFFLINE_THROTTLE
            } else {
                DEFAULT_THROTTLE
            };

            self.throttle = Box::pin(R::tick(throttle_duration).fuse());
        }

        fn reparse(&mut self) {
            if !self.offline {
                return;
            }

            #[cfg(feature = "local")]
            {
                self.tree = self
                    .parser
                    .parse(&self.input, None)
                    .expect("unexpected error while parsing");
            }
        }

        fn char_len(&self) -> usize {
            self.input.chars().count()
        }

        pub fn append<R: Runtime>(&mut self, ch: char) {
            if self.char_len() == self.cursor {
                self.input.push(ch);
            } else {
                let pos = self
                    .input
                    .char_indices()
                    .skip(self.cursor)
                    .next()
                    .unwrap()
                    .0;
                self.input.insert(pos, ch);
            }

            self.cursor += 1;
            self.reparse();
            self.throttle_on_input::<R>()
        }

        pub fn delete_word<R: Runtime>(&mut self) {
            let pos = self
                .input
                .char_indices()
                .skip(self.cursor)
                .next()
                .map(|(i, _)| i)
                .unwrap_or(self.char_len());

            let mut discarding = false;
            let mut index = 0;

            for (i, ch) in self.input[..pos].char_indices().rev() {
                match (ch.is_whitespace(), discarding) {
                    (true, false) => {}
                    (false, false) => {
                        discarding = true;
                    }
                    (false, true) => {}
                    (true, true) => {
                        index = i;
                        break;
                    }
                }
            }

            self.cursor = index;
            self.input.replace_range(index..pos, "");
            self.throttle_on_input::<R>();
            self.reparse()
        }

        pub fn delete<R: Runtime>(&mut self, back: bool) -> bool {
            let len = self.char_len();

            if !back {
                if self.cursor < len {
                    self.cursor += 1;
                } else {
                    return false;
                }
            }

            let changed = if len == self.cursor {
                if self.input.pop().is_some() {
                    self.cursor -= 1;
                    true
                } else {
                    false
                }
            } else if self.cursor > 0 {
                self.input.remove(self.cursor - 1);
                self.cursor -= 1;
                true
            } else {
                false
            };

            if changed {
                self.throttle_on_input::<R>();
                self.reparse();
            }

            changed
        }

        pub fn move_cursor(&mut self, step: i8) {
            let cursor = if step < 0 {
                self.cursor.saturating_sub(1)
            } else {
                (self.cursor + 1).min(self.char_len())
            };

            self.cursor = cursor;
        }

        // FIXME: this is not 100% correct
        pub fn move_cursor_word(&mut self, step: i8) {
            let len = self.char_len();

            if step > 0 {
                let next_space = self
                    .input
                    .char_indices()
                    .skip(self.cursor)
                    .filter(|&(_, ch)| ch == ' ')
                    .next()
                    .map(|(idx, _)| idx);

                self.cursor = next_space.map(|idx| idx + 1).unwrap_or(len).min(len);
            } else {
                let prev_space = self
                    .input
                    .char_indices()
                    .rev()
                    .skip(len - self.cursor)
                    .find(|&(_, ch)| ch == ' ')
                    .map(|(idx, _)| idx);

                self.cursor = prev_space.map(|idx| idx.saturating_sub(1)).unwrap_or(0);
            }
        }

        pub fn set_offline<R: Runtime>(&mut self, offline: bool) {
            let prev = self.offline;
            self.offline = offline;

            if !prev && offline {
                self.reparse();
            } else if prev && !offline {
                self.throttle = Box::pin(R::tick(0).fuse());
            }
        }
    }

    pub fn draw<'t, 's>(
        f: &'t mut Frame,
        state: &'s State,
        extra_state: &'s mut ExtraState,
        chunk: Rect,
    ) {
        #[cfg(feature = "local")]
        if state.offline {
            use ratatui::style::Modifier;

            let it = extra_state
                .highlighter
                .highlight(
                    &extra_state.highlighter_conf,
                    state.input.as_bytes(),
                    None,
                    |_| None,
                )
                .unwrap();

            let mut spans = vec![];
            let mut highlight = None;

            for event in it.filter_map(|ev| ev.ok()) {
                match event {
                    HighlightEvent::HighlightStart(hl) => {
                        highlight = Some(hl.0);
                    }
                    HighlightEvent::Source { start, end } => {
                        let content = &state.input[start..end];

                        let style = match highlight {
                            Some(0) => Style::default().fg(Color::Cyan),
                            Some(1) => Style::default().fg(Color::Yellow),
                            Some(2) => Style::default().add_modifier(Modifier::BOLD),
                            _ => Style::default(),
                        };

                        spans.push(Span::styled(content, style));
                    }
                    HighlightEvent::HighlightEnd => {
                        highlight = None;
                    }
                }
            }

            f.render_widget(Block::default().title(spans), chunk);
        } else {
            f.render_widget(Block::default().title(Span::raw(&state.input)), chunk);
        }

        #[cfg(not(feature = "local"))]
        {
            let _ = extra_state;
            f.render_widget(Block::default().title(Span::raw(&state.input)), chunk);
        }

        let cursor_pos: usize = state
            .input
            .chars()
            .take(state.cursor)
            .map(|ch| ch.width().unwrap_or(0))
            .sum();

        f.set_cursor(chunk.x + cursor_pos as u16, chunk.y);

        #[cfg(feature = "local")]
        let needs_syntax_error = state.offline
            && state.tree.root_node().has_error()
            && state.input.chars().any(|ch| !ch.is_whitespace());

        #[cfg(not(feature = "local"))]
        let needs_syntax_error = false;

        let error = Style::default().fg(Color::Red);

        if needs_syntax_error {
            let error_block = Block::default().style(error).title(" ");

            let mut chunk = chunk;
            chunk.width = 1;
            chunk.height = 1;

            let mut index = 0;
            for ch in state.input.chars().chain(std::iter::once(' ')) {
                if ch == ' ' {
                    let mut chunk = chunk;
                    chunk.x += index;
                    f.render_widget(error_block.clone(), chunk);
                }

                index += ch.width().unwrap_or(0) as u16;
            }
        }

        if needs_syntax_error {
            let mut chunk = chunk;
            let x = (chunk.x + state.char_len() as u16 + 2).max(chunk.width / 2);
            let content = "(invalid query)";

            chunk.height = 1;
            chunk.x = x;
            chunk.width = content.len() as u16;

            f.render_widget(
                Block::default().title(Span::raw(content)).style(error),
                chunk,
            );
        }
    }
}

pub mod spinner {
    use futures::{
        stream::{self, FusedStream},
        StreamExt as _,
    };

    use crate::{DynStream, Runtime};

    pub struct State {
        spinning: bool,
        frame: u8,
        stream: DynStream<()>,
    }

    impl State {
        pub fn new() -> Self {
            Self {
                spinning: false,
                frame: 0,
                stream: Box::pin(stream::empty()),
            }
        }

        pub fn toggle<R: Runtime>(&mut self, spin: bool) {
            let was_spinning = self.spinning;
            self.spinning = spin;

            match (was_spinning, self.spinning) {
                (false, true) => {
                    self.stream = Box::pin(R::ticks(45).fuse());
                    self.frame = 0;
                }
                (true, false) => {
                    self.stream = Box::pin(stream::empty());
                }
                _ => {}
            }
        }

        pub fn tick(&mut self) {
            self.frame += 1;
            self.frame %= 4;
        }

        pub fn stream(&mut self) -> impl FusedStream + '_ {
            &mut self.stream
        }

        pub fn icon(&self) -> &str {
            if !self.spinning {
                " "
            } else {
                ["|", "/", "-", "\\"][(self.frame % 4) as usize]
            }
        }
    }
}
