use anyhow::Result;
#[cfg(feature = "local")]
use tree_sitter::{Tree, TreeCursor};

#[cfg(feature = "local")]
pub fn tree_to_query(tree: &Tree, source: &str) -> Result<Query> {
    if tree.root_node().has_error() {
        anyhow::bail!("invalid tree {source:?}");
    }

    let mut cursor = tree.walk();

    cursor.goto_first_child();

    parse_query(&mut cursor, source)
}

#[cfg(feature = "local")]
fn parse_query(cursor: &mut TreeCursor, source: &str) -> Result<Query> {
    let simple_query = if cursor.node().kind() == "(" {
        assert!(cursor.goto_next_sibling());

        let simple = SimpleQuery::Wrapped(Box::new(parse_query(cursor, source)?));

        simple
    } else if cursor.node().kind() == "date_query" {
        assert!(cursor.goto_first_child());
        assert!(cursor.goto_next_sibling());

        let simple = SimpleQuery::Date(cursor.node().utf8_text(source.as_bytes())?.to_string());

        assert!(cursor.goto_parent());

        simple
    } else {
        let field: FieldKind = cursor.node().utf8_text(source.as_bytes())?.try_into()?;

        let mut terms = vec![];

        while cursor
            .node()
            .next_sibling()
            .map(|n| n.kind() == "identifier")
            .unwrap_or(false)
        {
            cursor.goto_next_sibling();
            terms.push(cursor.node().utf8_text(source.as_bytes())?.to_string());
        }

        SimpleQuery::Field(field, terms)
    };

    if !cursor.goto_next_sibling() {
        return Ok(Query {
            simple: simple_query,
            compound: None,
        });
    }

    let operator: Operator = cursor
        .node()
        .utf8_text(source.as_bytes())?
        .try_into()
        .unwrap();

    assert!(cursor.goto_next_sibling());

    let mut subcursor = cursor.node().walk();
    assert!(subcursor.goto_first_child());

    cursor.goto_next_sibling();

    Ok(Query {
        simple: simple_query,
        compound: Some((operator, Box::new(parse_query(&mut subcursor, source)?))),
    })
}

impl<'a> TryFrom<&'a str> for Operator {
    type Error = &'a str;

    fn try_from(value: &'a str) -> Result<Self, Self::Error> {
        match &value.to_lowercase()[..] {
            "or" | "|" => Ok(Operator::Or),
            "and" | "&" | "+" => Ok(Operator::And),
            _ => Err(value),
        }
    }
}

impl TryFrom<&str> for FieldKind {
    type Error = anyhow::Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match &value.to_lowercase()[..] {
            "a" | "au" | "author" => Ok(FieldKind::Author),
            "t" | "ti" | "title" => Ok(FieldKind::Title),
            _ => anyhow::bail!("invalid field {:?}", value),
        }
    }
}

#[derive(Debug)]
pub struct Query {
    pub simple: SimpleQuery,
    pub compound: Option<(Operator, Box<Query>)>,
}

#[derive(Debug)]
pub enum SimpleQuery {
    Wrapped(Box<Query>),
    Field(FieldKind, Vec<String>),
    Date(String),
}

#[derive(Debug)]
pub enum Operator {
    And,
    Or,
}

#[derive(Debug)]
pub enum FieldKind {
    Author,
    Title,
}

#[test]
fn foo() {
    let mut parser = tree_sitter::Parser::new();

    parser
        .set_language(tree_sitter_inspire_query::language())
        .unwrap();

    let source = "find AU 2003";

    let tree = parser.parse(source, None).unwrap();

    let query = tree_to_query(&tree, source);

    println!("{:?}", query);

    let mut conf = tree_sitter_highlight::HighlightConfiguration::new(
        tree_sitter_inspire_query::language(),
        "(field) @foobar",
        "",
        "",
    )
    .unwrap();

    let mut highlighter = tree_sitter_highlight::Highlighter::new();

    conf.configure(&["foobar"]);

    let events: Vec<_> = highlighter
        .highlight(&conf, source.as_bytes(), None, |_| None)
        .unwrap()
        .collect();

    println!("{:?}", events);

    panic!("boom");
}
