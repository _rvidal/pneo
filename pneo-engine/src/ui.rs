use std::io;

use ratatui::{
    layout::{Constraint, Layout, Rect},
    style::{Color, Modifier, Style, Stylize as _},
    text::{Line, Span},
    widgets::{Block, Borders, Paragraph, Row, Scrollbar, ScrollbarState, Table},
    Frame, Terminal,
};

use crate::{Download, ExtraState, Overlay, State};

pub fn ui<'t, 's, B: ratatui::backend::Backend>(
    terminal: &'t mut Terminal<B>,
    state: &'s mut State,
    extra_state: &'s mut ExtraState,
    redraw: bool,
) -> Result<ratatui::CompletedFrame<'t>, io::Error> {
    if redraw {
        terminal.clear()?;
    }

    terminal.draw(|f| draw_ui(f, state, extra_state))
}

fn draw_ui<'t, 's>(f: &'t mut Frame, state: &'s mut State, extra_state: &'s mut ExtraState) {
    let size = f.size();

    if size.height < 6 || size.width < 32 {
        let message = Layout::vertical([Constraint::Percentage(50), Constraint::Percentage(50)])
            .split(size)[1];
        f.render_widget(
            Paragraph::new("Terminal is too small. Please, resize it.")
                .wrap(ratatui::widgets::Wrap { trim: false }),
            message,
        );
        return;
    }

    // RENDER: app border
    let block = Block::default().borders(Borders::ALL).title(Span::styled(
        "πνέω",
        Style::default().add_modifier(Modifier::BOLD),
    ));
    f.render_widget(block, size);

    // RENDER: input
    let input_block = Block::default().borders(Borders::ALL);

    let chunks = Layout::vertical([Constraint::Length(3), Constraint::Min(0)])
        .margin(1)
        .split(size);

    let input_chunk = chunks[0];
    let results_chunk = chunks[1];
    f.render_widget(input_block, input_chunk);

    const TEXT_TOP_MARGIN: u16 = 1;
    const TEXT_LEFT_MARGIN: u16 = 5;

    let mut text_chunk = input_chunk;
    text_chunk.y += TEXT_TOP_MARGIN;
    text_chunk.height -= TEXT_TOP_MARGIN;
    text_chunk.x += TEXT_LEFT_MARGIN;
    text_chunk.width -= TEXT_LEFT_MARGIN;

    crate::components::input::draw(f, &state.input, &mut extra_state.input, text_chunk);

    // RENDER: spinner/offline
    const SPINNER_X_DIFF: u16 = 3;
    const SPINNER_WIDTH: u16 = 4;
    let mut spinner_chunk = text_chunk;
    spinner_chunk.x -= SPINNER_X_DIFF;
    spinner_chunk.width = SPINNER_WIDTH;

    f.render_widget(
        Block::default().title(if state.offline() {
            Span::styled("✈", Style::default().fg(Color::Red))
        } else {
            Span::raw(state.spinner.icon())
        }),
        spinner_chunk,
    );

    // RENDER: progress bar
    draw_progress_bar(f, &state.progress, input_chunk);

    // RENDER: table messages
    let message_chunk = Layout::vertical([Constraint::Min(0)])
        .margin(1)
        .split(results_chunk)[0];

    let busy = state.busy();

    let table = match &mut state.output {
        None => None,
        Some(Ok(res)) => {
            if res.entries.is_empty() {
                f.render_widget(
                    Block::default().title("No results".to_string()),
                    message_chunk,
                );
                None
            } else {
                Some(res)
            }
        }
        Some(Err(err)) => {
            log::error!("{err:?}");
            f.render_widget(Paragraph::new(format!("{err:?}")), message_chunk);
            None
        }
    };

    // RENDER: table
    if let Some(table) = table {
        let mut results_chunk = results_chunk;
        let (offset, marker) =
            table.draw(results_chunk.height as usize, &mut extra_state.table.offset);

        let rows = table
            .entries
            .iter()
            .skip(offset)
            .take(results_chunk.height as usize)
            .map(|article| {
                let (reference, download) = {
                    let reference = article
                        .eprint()
                        .map(|eprint| eprint.to_string())
                        .unwrap_or_else(|| format!("{}", article.control_number));

                    let download = state.downloaded.get(&reference).copied();

                    (reference, download)
                };

                let downloaded = download.is_some();

                let eprint = article.eprint().map(|eprint| {
                    if let Some(Download::Arxiv(version)) = download {
                        format!("{}v{}", eprint, version)
                    } else {
                        eprint.to_string()
                    }
                });

                let has_eprint = eprint.is_some();

                let reference = eprint.unwrap_or_else(|| format!("⚬ins:{}", reference));
                let title = article.title.as_deref().unwrap_or("(No title)");
                let authors = article.authors();

                ((downloaded, has_eprint), title, reference, authors)
            })
            .chain(
                std::iter::once_with(|| ((false, false), "", "".to_string(), "".to_string()))
                    .filter(|_| table.page.is_some()),
            )
            .enumerate()
            .map(
                |(i, ((downloaded, has_eprint), title, reference, authors))| {
                    let highlight = i == marker;
                    let mark = !busy && highlight;

                    Row::new(vec![
                        format!(
                            "{} {} {}",
                            if mark { ">" } else { " " },
                            if downloaded { "✔" } else { " " },
                            title
                        )
                        .into(),
                        ratatui::widgets::Cell::new(reference).style(if downloaded || has_eprint {
                            Style::default()
                        } else {
                            Style::default().dim()
                        }),
                        authors.into(),
                    ])
                    .style(if highlight {
                        Style::default()
                            .bg(Color::LightBlue)
                            .add_modifier(Modifier::BOLD)
                            .fg(Color::Black)
                    } else {
                        Style::default()
                    })
                },
            )
            .collect::<Vec<_>>();

        extra_state.table.size = results_chunk;

        const SCROLLBAR_WIDTH: u16 = 1;
        const SCROLLBAR_MARGIN: u16 = 1;

        results_chunk.width -= SCROLLBAR_MARGIN + SCROLLBAR_WIDTH;

        f.render_widget(
            Table::new(
                rows,
                &[
                    Constraint::Percentage(65),
                    Constraint::Percentage(10),
                    Constraint::Percentage(25),
                ],
            ),
            results_chunk,
        );

        if table.page.is_some() && offset + (results_chunk.height as usize) >= table.ui_len() {
            let mut more_chunk = results_chunk;
            more_chunk.y += (more_chunk.height).min(table.ui_len() as u16) - 1;
            more_chunk.height = 1;

            f.render_widget(
                Paragraph::new("▼▼  More results  ▼▼")
                    .alignment(ratatui::layout::Alignment::Center),
                more_chunk,
            );
        }

        // RENDER: scrollbar
        let mut scrollbar_chunk = results_chunk;
        scrollbar_chunk.x += scrollbar_chunk.width + SCROLLBAR_MARGIN;
        scrollbar_chunk.width = SCROLLBAR_WIDTH;

        f.render_stateful_widget(
            Scrollbar::new(ratatui::widgets::ScrollbarOrientation::VerticalRight),
            scrollbar_chunk,
            &mut ScrollbarState::default()
                .position(table.focus)
                .viewport_content_length(results_chunk.height as usize)
                .content_length(table.ui_len()),
        );
    }

    if state.output.is_none() && state.help {
        f.render_widget(
            Paragraph::new("Ctrl+h for help"),
            results_chunk.inner(ratatui::layout::Margin {
                horizontal: 4,
                vertical: 2,
            }),
        );
    }

    // RENDER: popup
    if let Some(overlay @ Overlay::Message { .. }) = &state.overlay {
        draw_popup(f, overlay, extra_state, size);
    }

    // RENDER: abstract
    'abs: {
        if !std::matches![state.overlay, Some(Overlay::Abstract)] {
            break 'abs;
        }

        let Some(article) = state
            .output
            .as_ref()
            .and_then(|res| res.as_ref().ok())
            .and_then(|table| table.entries.get(table.focus as usize))
        else {
            break 'abs;
        };

        let chunks = Layout::horizontal([
            Constraint::Min(0),
            Constraint::Percentage(60),
            Constraint::Min(0),
        ])
        .split(size);

        let chunk = chunks[1].inner(ratatui::prelude::Margin {
            horizontal: 0,
            vertical: 3,
        });

        let content_chunk = chunk.inner(ratatui::prelude::Margin {
            horizontal: 2,
            vertical: 1,
        });

        let content = {
            let mut content = vec![Line::styled(
                article.title.as_deref().unwrap_or("(No title)"),
                Style::default().add_modifier(Modifier::BOLD),
            )];

            if let Some(abstract_) = article.abstract_() {
                content.push(Line::from("\n"));
                content.push(Line::from(abstract_));
            }

            content
        };

        extra_state.overlay = chunk;
        f.render_widget(ratatui::widgets::Clear, chunk);
        f.render_widget(
            Block::default()
                .border_type(ratatui::widgets::BorderType::Double)
                .borders(Borders::all()),
            chunk.inner(ratatui::prelude::Margin {
                horizontal: 1,
                vertical: 0,
            }),
        );
        f.render_widget(
            ratatui::widgets::Paragraph::new(content).wrap(Default::default()),
            content_chunk,
        );
    }

    // RENDER: help
    if let Some(Overlay::Help) = state.overlay {
        let h_chunk = Layout::horizontal([
            Constraint::Min(0),
            Constraint::Percentage(60),
            Constraint::Min(0),
        ])
        .split(size)[1];

        let help = r#"
🠉, 🠋          select entry in results list
Enter         open article
Esc           close dialogs, cancel downloads, quit application
PgUp, PgDown  jump through results list
Ctrl+🠈/🠊      jump words in search input
Ctrl+w        delete word in search input
Ctrl+r        refresh screen
Ctrl+o        toggle offline mode (✈)
Ctrl+a        toggle abstract view
Ctrl+b        copy Bibtex reference
Ctrl+p        open entry in INSPIRE
Ctrl+h        show this help"#;

        let chunk = Layout::vertical([
            Constraint::Fill(1),
            Constraint::Min(help.lines().count() as u16 + 4),
            Constraint::Fill(1),
        ])
        .split(h_chunk)[1];

        extra_state.overlay = chunk;

        let content_v_chunk =
            Layout::vertical([Constraint::Max(1), Constraint::Fill(1), Constraint::Max(1)])
                .split(chunk)[1];

        let content_chunk = content_v_chunk.inner(ratatui::prelude::Margin {
            horizontal: 2,
            vertical: 0,
        });

        f.render_widget(ratatui::widgets::Clear, chunk);
        f.render_widget(
            Block::default()
                .title("Help")
                .border_type(ratatui::widgets::BorderType::Double)
                .borders(Borders::all()),
            chunk,
        );
        let lines = help.lines().map(Line::from).collect::<Vec<_>>();
        f.render_widget(Paragraph::new(lines), content_chunk);
    }
}

fn draw_popup<'t, 's>(
    f: &'t mut Frame,
    overlay: &'s Overlay,
    extra: &'s mut ExtraState,
    chunk: Rect,
) {
    let Overlay::Message {
        contents: ref warning,
        error: is_error,
    } = *overlay
    else {
        return;
    };

    let chunks = Layout::horizontal([
        Constraint::Percentage(25),
        Constraint::Percentage(50),
        Constraint::Min(0),
    ])
    .split(chunk);

    let chunk = chunks[1];

    const PADDING: u16 = 1;

    let warning_rect = {
        let width = chunk.width;
        const MARGIN: u16 = 1 + PADDING;

        let effective_width = width - 2 * MARGIN;
        let effective_lines = warning.lines().fold(0, |acc, line| {
            acc + (line.len() as u16 / effective_width) + 1
        });

        let desired_height = effective_lines + 2 * MARGIN;

        let rect = Layout::vertical([
            Constraint::Fill(1),
            Constraint::Min(desired_height),
            Constraint::Fill(1),
        ])
        .split(chunk)[1];

        rect
    };

    extra.overlay = warning_rect;
    f.render_widget(ratatui::widgets::Clear, warning_rect);

    let outer_block = Block::default()
        .borders(Borders::ALL)
        .border_type(ratatui::widgets::BorderType::Double)
        .border_style(Style::default().bg(if is_error {
            Color::LightRed
        } else {
            Color::LightGreen
        }));

    let inner = {
        let mut inner = outer_block.inner(warning_rect);

        inner.x += PADDING;
        inner.y += PADDING;
        inner.height -= PADDING;
        inner.width -= PADDING;
        inner
    };

    f.render_widget(outer_block, warning_rect);

    f.render_widget(
        Paragraph::new(&warning[..]).wrap(ratatui::widgets::Wrap { trim: false }),
        inner,
    );
}

fn draw_progress_bar<'t, 's>(f: &'t mut Frame, state: &'s Option<(usize, usize)>, chunk: Rect) {
    let Some(progress) = state.filter(|(_, total)| *total > 0) else {
        return;
    };

    let progress_chunk =
        Layout::horizontal([Constraint::Percentage(20), Constraint::Percentage(20)]).split(chunk)
            [1];

    f.render_widget(
        Block::default()
            .border_type(ratatui::widgets::BorderType::Double)
            .borders(Borders::ALL),
        progress_chunk,
    );

    let total = progress.0 as f32 / progress.1 as f32;

    let width = ((progress_chunk.width - 2) as f32 * total).floor() as u16;

    f.render_widget(
        Block::default().style(Style::default().bg(Color::Green)),
        Rect {
            x: progress_chunk.x + 1,
            y: progress_chunk.y + 1,
            width,
            height: 1,
        },
    )
}
