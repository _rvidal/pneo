use std::{
    cell::RefCell,
    collections::HashMap,
    io::Write,
    rc::Rc,
    sync::Arc,
    time::{Duration, SystemTime},
};

use anyhow::Result;
use js_sys::Uint8Array;
use pneo_engine::{main_loop, preprint, Cache, Download, Runtime, Store, VersionChecker};
use ratatui::{
    backend::{Backend, CrosstermBackend},
    crossterm::{
        self,
        event::{parser, InternalEvent},
    },
    Terminal,
};
use wasm_bindgen::prelude::*;

use futures::{
    channel::mpsc::{self, UnboundedSender},
    future::FutureExt as _,
    stream::StreamExt as _,
};
use wasm_bindgen_futures::{spawn_local, stream::JsStream};

#[wasm_bindgen(module = "/js/glue.js")]
extern "C" {
    fn js_tick(millis: u32) -> js_sys::Promise;
    fn xterm_js_write(instance: &JsValue, data: &[u8]);
    fn xterm_js_reader(
        instance: &JsValue,
        ctrl: &web_sys::AbortController,
    ) -> js_sys::AsyncIterator;
    fn xterm_js_on_resize(
        instance: &JsValue,
        ctrl: &web_sys::AbortController,
    ) -> js_sys::AsyncIterator;
}

#[derive(Clone)]
struct XTermJsWriter {
    instance: JsValue,
}

impl Write for XTermJsWriter {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        xterm_js_write(&self.instance, buf);
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

async fn do_parse(mut it: JsStream, tx: UnboundedSender<Result<InternalEvent>>) {
    let mut buffer = vec![];
    let mut parsing_buffer = vec![];

    loop {
        let Some(value) = it.next().await else {
            return;
        };

        let Ok(value) = value else {
            let _ = tx.unbounded_send(Err(anyhow::anyhow!("error on stream")));
            return;
        };

        let Ok(data) = value.dyn_into::<Uint8Array>() else {
            let _ = tx.unbounded_send(Err(anyhow::anyhow!("not a byte array")));
            return;
        };

        buffer.resize(data.byte_length() as usize, 0);

        data.copy_to(&mut buffer);

        for (i, b) in buffer.iter().copied().enumerate() {
            let last = i == buffer.len() - 1;
            parsing_buffer.push(b);

            match parser::parse_event(&parsing_buffer, !last) {
                Ok(Some(event)) => {
                    if tx.unbounded_send(Ok(event)).is_err() {
                        return;
                    }
                    parsing_buffer.clear();
                }
                Ok(None) => {
                    continue;
                }
                Err(err) => {
                    let _ = tx.unbounded_send(Err(anyhow::anyhow!("parse error: {err:?}")));
                    return;
                }
            };
        }
    }
}

struct XTermJsBackend {
    crossterm: CrosstermBackend<XTermJsWriter>,
    instance: JsValue,
}

impl Backend for XTermJsBackend {
    fn draw<'a, I>(&mut self, content: I) -> std::io::Result<()>
    where
        I: Iterator<Item = (u16, u16, &'a ratatui::prelude::buffer::Cell)>,
    {
        self.crossterm.draw(content)
    }

    fn hide_cursor(&mut self) -> std::io::Result<()> {
        self.crossterm.hide_cursor()
    }

    fn show_cursor(&mut self) -> std::io::Result<()> {
        self.crossterm.show_cursor()
    }

    fn get_cursor(&mut self) -> std::io::Result<(u16, u16)> {
        self.crossterm.get_cursor()
    }

    fn set_cursor(&mut self, x: u16, y: u16) -> std::io::Result<()> {
        self.crossterm.set_cursor(x, y)
    }

    fn clear(&mut self) -> std::io::Result<()> {
        self.crossterm.clear()
    }

    fn size(&self) -> std::io::Result<ratatui::prelude::Rect> {
        let (cols, rows) = xterm_js_size(&self.instance);

        Ok(ratatui::prelude::Rect {
            x: 0,
            y: 0,
            width: cols,
            height: rows,
        })
    }

    fn window_size(&mut self) -> std::io::Result<ratatui::prelude::backend::WindowSize> {
        let size = self.size()?;
        Ok(ratatui::prelude::backend::WindowSize {
            columns_rows: ratatui::layout::Size {
                width: size.width,
                height: size.height,
            },
            // TODO
            pixels: Default::default(),
        })
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Backend::flush(&mut self.crossterm)
    }
}

#[wasm_bindgen]
pub async fn main(instance: JsValue) {
    console_log::init().unwrap();
    console_error_panic_hook::set_once();

    let ctrl = web_sys::AbortController::new().unwrap();

    let mut writer = XTermJsWriter {
        instance: instance.clone(),
    };

    crossterm::execute!(
        writer,
        crossterm::terminal::EnterAlternateScreen,
        crossterm::event::EnableMouseCapture
    )
    .unwrap();

    let backend = CrosstermBackend::new(writer.clone());

    let backend = XTermJsBackend {
        crossterm: backend,
        instance: instance.clone(),
    };

    let mut terminal = Terminal::new(backend).unwrap();

    let (tx, rx) = mpsc::unbounded();

    let it = xterm_js_reader(&instance, &ctrl);

    {
        let tx = tx.clone();
        spawn_local(do_parse(it.into(), tx));
    }

    spawn_local({
        let instance = instance.clone();
        let ctrl = ctrl.clone();

        async move {
            let mut stream = JsStream::from(xterm_js_on_resize(&instance, &ctrl));

            while let Some(item) = stream.next().await {
                let payload = match item.ok().and_then(|val| val.as_f64()) {
                    Some(payload) => payload as u32,
                    None => break,
                };

                let cols = (payload >> 16) as u16;
                let rows = (payload & 0xFFFF) as u16;

                if tx
                    .unbounded_send(Ok(InternalEvent::Event(crossterm::event::Event::Resize(
                        cols, rows,
                    ))))
                    .is_err()
                {
                    return;
                }
            }
        }
    });

    let stream = rx
        .take_while(|item| std::future::ready(item.is_ok()))
        .map(|item| item.unwrap())
        .filter_map(|ev| {
            std::future::ready(match ev {
                InternalEvent::Event(event) => Some(Ok::<_, std::io::Error>(event)),
                InternalEvent::CursorPosition(_, _)
                | InternalEvent::KeyboardEnhancementFlags(_)
                | InternalEvent::PrimaryDeviceAttributes => None,
            })
        });

    let result = main_loop(
        &mut terminal,
        WebRuntime {
            cache: Default::default(),
            store: Some(Default::default()),
        },
        stream,
    )
    .await;

    ctrl.abort();

    crossterm::execute!(
        writer,
        crossterm::terminal::LeaveAlternateScreen,
        crossterm::event::DisableMouseCapture,
        crossterm::style::SetForegroundColor(crossterm::style::Color::Reset)
    )
    .unwrap();

    let Err(err) = result else {
        return;
    };

    terminal.hide_cursor().unwrap();
    terminal.clear().unwrap();

    write!(writer, "\r\n\r\nThere was an error running pneo:\r\n\r\n").unwrap();

    crossterm::execute!(
        writer,
        crossterm::style::SetForegroundColor(crossterm::style::Color::Reset)
    )
    .unwrap();

    write!(writer, "\t{err}\r\n").unwrap();
}

struct WebRuntime {
    cache: Arc<WebCache>,
    store: Option<WebStore>,
}

#[derive(Default)]
struct WebCache {
    by_referenced: RefCell<HashMap<String, Rc<(String, u8)>>>,
    by_id: RefCell<HashMap<String, Rc<(String, u8)>>>,
}

impl Cache for WebCache {
    fn preprint_file_from_id(&self, id: &str) -> Result<Option<std::path::PathBuf>> {
        Ok(self.by_id.borrow().get(id).map(|s| (&**s).clone().0.into()))
    }

    fn has(
        &self,
        _id: &str,
        referenced_id: &str,
        _url: &str,
    ) -> Result<Option<std::path::PathBuf>> {
        Ok(self
            .by_referenced
            .borrow()
            .get(referenced_id)
            .map(|s| (&**s).clone().0.into()))
    }

    fn insert(
        &self,
        id: &str,
        referenced_id: &str,
        url: &str,
        content: Vec<u8>,
    ) -> Result<std::path::PathBuf> {
        let (_, version) = preprint::validate(id, referenced_id, url)?;

        let u8array = Uint8Array::new_with_length(content.len() as u32);
        u8array.copy_from(&content);

        let array = js_sys::Array::new();
        array.push(&u8array);

        let options = web_sys::BlobPropertyBag::new();
        options.set_type("application/pdf");

        let blob = web_sys::Blob::new_with_u8_array_sequence_and_options(&array, &options).unwrap();

        let url = web_sys::Url::create_object_url_with_blob(&blob).unwrap();
        let value = Rc::new((url, version));

        self.by_id.borrow_mut().insert(id.into(), value.clone());
        self.by_referenced
            .borrow_mut()
            .insert(referenced_id.into(), value.clone());

        Ok((&*value.0).into())
    }

    fn get_downloaded(&self) -> Result<std::collections::HashMap<String, pneo_engine::Download>> {
        Ok(self
            .by_referenced
            .borrow()
            .clone()
            .into_iter()
            .map(|(k, v)| (k, Download::Arxiv(v.1)))
            .collect())
    }

    fn get_recid(&self, _recid: u32) -> Result<Option<std::path::PathBuf>> {
        Ok(None)
    }

    fn insert_recid(&self, _recid: u32, _file: &std::ffi::OsString) -> anyhow::Result<()> {
        Ok(())
    }
}

#[derive(Default)]
struct WebStore {
    // articles: HashMap<u32, Article>,
}

impl Store for WebStore {
    // fn upsert(&mut self, article: &Article) -> Result<()> {
    //     let article = article.clone();
    //     self.articles.insert(article.control_number, article);
    //     Ok(())
    // }

    // fn query(&self, query: pneo_engine::syntax::Query) -> Result<Vec<Article>> {
    //     let articles = self
    //         .articles
    //         .values()
    //         .filter(|article| filter_by_query(&query, article))
    //         .cloned()
    //         .collect();

    //     Ok(articles)
    // }
}

// fn filter_by_query(query: &Query, article: &Article) -> bool {
//     let simple = match &query.simple {
//         SimpleQuery::Wrapped(q) => filter_by_query(&*q, article),
//         SimpleQuery::Field(kind, values) => match kind {
//             FieldKind::Author => values
//                 .iter()
//                 .all(|value| article.authors.iter().any(|author| author.contains(value))),
//             FieldKind::Title => values.iter().all(|value| {
//                 article
//                     .title
//                     .as_ref()
//                     .map(|title| title.contains(value))
//                     .unwrap_or(false)
//             }),
//         },
//         SimpleQuery::Date(date) => article.created.starts_with(&format!("{date}-")),
//     };

//     if let Some((op, nested)) = &query.compound {
//         let compound = filter_by_query(&*nested, article);

//         match op {
//             Operator::And => simple && compound,
//             Operator::Or => simple || compound,
//         }
//     } else {
//         simple
//     }
// }

struct WebVersionChecker {}

impl VersionChecker for WebVersionChecker {}

impl Runtime for WebRuntime {
    type Cache = WebCache;

    type Store = WebStore;

    type VersionChecker = WebVersionChecker;

    fn cache(&self) -> std::sync::Arc<Self::Cache> {
        self.cache.clone()
    }

    fn store(&mut self) -> Option<Self::Store> {
        self.store.take()
    }

    fn version_checker(&self) -> Self::VersionChecker {
        WebVersionChecker {}
    }

    fn open(&self, uri: impl AsRef<std::ffi::OsStr> + std::fmt::Debug) -> Result<()> {
        let Some(window) = web_sys::window() else {
            return Ok(());
        };

        let _ = window.open_with_url(&uri.as_ref().to_string_lossy());

        Ok(())
    }

    fn copy(&self, content: &str) -> Result<()> {
        let Some(window) = web_sys::window() else {
            return Ok(());
        };

        let _ = window.navigator().clipboard().write_text(content);

        Ok(())
    }

    fn offline(&self) -> bool {
        false
    }

    fn tick(millis: u64) -> impl std::future::Future<Output = ()> + 'static {
        wasm_bindgen_futures::JsFuture::from(js_tick(millis as u32)).map(|_| ())
    }

    fn now() -> std::time::SystemTime {
        SystemTime::UNIX_EPOCH + Duration::from_millis(js_sys::Date::now() as u64)
    }

    fn can_stop() -> bool {
        false
    }
}

fn xterm_js_size(instance: &JsValue) -> (u16, u16) {
    let cols = js_sys::Reflect::get(instance, &JsValue::from_str("cols"))
        .unwrap()
        .as_f64()
        .unwrap() as u16;
    let rows = js_sys::Reflect::get(instance, &JsValue::from_str("rows"))
        .unwrap()
        .as_f64()
        .unwrap() as u16;

    (cols, rows)
}
