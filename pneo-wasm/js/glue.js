
  const encoder = new TextEncoder();
  const decoder = new TextDecoder();

/**
 * @param {number} millis
 */
export function js_tick(millis) {
  return new Promise((res) => {
    setTimeout(() => res(), millis);
  });
}

export function xterm_js_write(instance, data) {
  instance.write(decoder.decode(data));
}

export function xterm_js_reader(instance, ctrl) {
  let deferred = Promise.withResolvers();
  let buffers = [];

  async function *loop() {
    while (!ctrl.signal.aborted) {
      if (buffers.length) {
        let result;

        if (buffers.length > 1) {
          const len = buffers.reduce((acc, b) => acc + b.byteLength, 0);
          result = new Uint8Array(len);

          let offset = 0;

          for (const buf  of buffers) {
            result.set(buf, offset);
            offset += buf.byteLength;
          }
        } else {
          result = buffers[0];
        }

        buffers = [];

        yield result;

        continue;
      }

      deferred = Promise.withResolvers();

      await deferred.promise;
    }

    disposable.dispose();
  }
  
  const disposable = instance.onData((data) => {
    buffers.push(encoder.encode(data));
    deferred.resolve();
  });

  return loop();
}

export function xterm_js_on_resize(instance, ctrl) {
  let deferred = Promise.withResolvers();

  async function *loop() {
    while (!ctrl.signal.aborted) {
      await deferred.promise;

      const cols = instance.cols;
      const rows = instance.rows;

      const payload = cols << 16 + rows;
      yield payload;
    }

    disposable.dispose();
  }

  const disposable = instance.onResize(() => {
    deferred.resolve();

    deferred = Promise.withResolvers();
  });

  return loop();
}
